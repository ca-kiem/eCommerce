﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Shopping.Aggregator.Extensions
{
    public static class HttpClientExtension
    {
        public static async Task<T> ReadContentAsAsync<T>(this HttpResponseMessage message)
        {
            if (!message.IsSuccessStatusCode)
            {
                throw new ApplicationException("Something went wrong when calling api: " + message.ReasonPhrase);
            }
            else
            {
                var dataAsStr = await message.Content.ReadAsStringAsync().ConfigureAwait(continueOnCapturedContext: false);

                return JsonSerializer.Deserialize<T>(dataAsStr, new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                });
            }
        }
    }
}
