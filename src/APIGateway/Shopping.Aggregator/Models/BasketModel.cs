﻿using System.Collections.Generic;

namespace Shopping.Aggregator.Models
{
    public class BasketModel
    {
        public BasketModel()
        {
            Items = new List<BasketItemExtendedModel>();
        }
        public string UserId { get; set; }
        public IList<BasketItemExtendedModel> Items { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
