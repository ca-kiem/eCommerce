﻿namespace Shopping.Aggregator.Models
{
    public class ShoppingModel
    {
        public string UserId { get; set; }
        /// <summary>
        /// Basket model with products
        /// </summary>
        public BasketModel BasketModel { get; set; }
    }
}
