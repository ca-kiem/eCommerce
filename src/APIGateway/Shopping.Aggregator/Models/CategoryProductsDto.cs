﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shopping.Aggregator.Models
{
    public class CategoryProductsDto
    {
        public CategoryProductsDto()
        {
            ProductIds = new List<string>();
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<string> ProductIds { get; set; }
    }
}
