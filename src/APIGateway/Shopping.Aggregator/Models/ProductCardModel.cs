﻿using System;

namespace Shopping.Aggregator.Models
{
    public class ProductCardModel
    {
        public string Id { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public bool Status { get; set; }
        public decimal Sale { get; set; }
        public string ImagePath { get; set; }
    }
}
