﻿using System.Collections.Generic;

namespace Shopping.Aggregator.Models
{
    public class CategoryProductsModel
    {
        public CategoryProductsModel()
        {
            Products = new List<ProductCardModel>();
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<ProductCardModel> Products { get; set; }
    }
}
