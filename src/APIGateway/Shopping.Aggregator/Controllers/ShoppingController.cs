﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Shopping.Aggregator.Models;
using Shopping.Aggregator.Services;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Shopping.Aggregator.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ShoppingController : ControllerBase
    {
        private readonly IBasketService _basketService;
        private readonly IProductService _productService;
        private readonly ISystemService _systemService;
        private readonly ILogger<ShoppingController> _logger;

        public ShoppingController(IBasketService basketService, ILogger<ShoppingController> logger, ISystemService systemService, IProductService productService)
        {
            _basketService = basketService;
            _logger = logger;
            _systemService = systemService;
            _productService = productService;
        }

        [HttpGet("{userId}", Name = "GetShopping")]
        [ProducesResponseType(typeof(BasketModel), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<BasketModel>> GetShopping(string userId)
        {
            // get basket 
            var basket = await _basketService.GetBasketAsync(userId);
            // get addition product information of the basket item
            var products = await _productService.GetProductsAsync(basket.Items.Select(x => x.ProductId));
            foreach (var item in basket.Items)
            {
                var product = products.FirstOrDefault(x => x.Id == item.ProductId);
                if (product != null)
                {
                    item.ProductName = product.Name;
                    item.Category = product.Category;
                    item.Description = product.Description;
                    item.Summary = product.Summary;
                    item.ImageFile = product.ImageFile;
                    item.Price = product.Price;
                    item.Sale = product.Sale;
                }
            }

            return Ok(basket);
        }

        [HttpGet("CategoryProducts", Name = "GetCategoryProducts")]
        [ProducesResponseType(typeof(IEnumerable<CategoryProductsModel>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<CategoryProductsModel>>> GetCategoryProducts()
        {
            _logger.LogInformation("[ShoppingController-Aggregator] Get category products");
            var categoryProducts = await _systemService.GetCategoryProductsAsync();
            var lstProductIds = categoryProducts.SelectMany(x => x.ProductIds).Distinct();

            // get all product with ids
            _logger.LogInformation("[ShoppingController-Aggregator] Get category products with ids: " + string.Join(", ", lstProductIds));
            var products = await _productService.GetProductsCardAsync(lstProductIds);

            return Ok(from category in categoryProducts
                      select new CategoryProductsModel
                      {
                          Id = category.Id,
                          Name = category.Name,
                          Products = products.Where(x => category.ProductIds.ToList().Contains(x.Id))
                      });
        }
    }
}
