﻿using Shopping.Aggregator.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shopping.Aggregator.Services
{
    public interface IProductService
    {
        Task<IEnumerable<ProductCardModel>> GetProductsCardAsync(IEnumerable<string> ids);
        Task<IEnumerable<ProductModel>> GetProductsAsync(IEnumerable<string> ids);
    }
}
