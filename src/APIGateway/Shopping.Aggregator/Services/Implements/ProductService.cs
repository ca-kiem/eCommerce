﻿using Shopping.Aggregator.Extensions;
using Shopping.Aggregator.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Shopping.Aggregator.Services.Implements
{
    public class ProductService : IProductService
    {
        private readonly HttpClient _client;

        public ProductService(HttpClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<ProductCardModel>> GetProductsCardAsync(IEnumerable<string> ids)
        {
            HttpContent httpContent = new StringContent(JsonSerializer.Serialize(ids), Encoding.UTF8, "application/json");
            
            var response = await _client.PostAsync($"/api/v1/Product/GetProductsCardByIds", httpContent);
            return await response.ReadContentAsAsync<IEnumerable<ProductCardModel>>();
        }
        
        public async Task<IEnumerable<ProductModel>> GetProductsAsync(IEnumerable<string> ids)
        {
            HttpContent httpContent = new StringContent(JsonSerializer.Serialize(ids), Encoding.UTF8, "application/json");
            
            var response = await _client.PostAsync($"/api/v1/Product/GetProductsByIds", httpContent);
            return await response.ReadContentAsAsync<IEnumerable<ProductModel>>();
        }
    }
}
