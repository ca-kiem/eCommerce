﻿using Shopping.Aggregator.Extensions;
using Shopping.Aggregator.Models;

using System.Net.Http;
using System.Threading.Tasks;

namespace Shopping.Aggregator.Services.Implements
{
    public class BasketService : IBasketService
    {
        private readonly HttpClient _client;

        public BasketService(HttpClient client)
        {
            _client = client;
        }

        public async Task<BasketModel> GetBasketAsync(string userId)
        {
            var response = await _client.GetAsync($"/api/v1/basket/{userId}");
            return await response.ReadContentAsAsync<BasketModel>();
        }
    }
}
