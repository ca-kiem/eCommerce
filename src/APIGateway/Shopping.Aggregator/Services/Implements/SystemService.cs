﻿using Shopping.Aggregator.Extensions;
using Shopping.Aggregator.Models;

using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Shopping.Aggregator.Services.Implements
{
    public class SystemService : ISystemService
    {
        private readonly HttpClient _client;

        public SystemService(HttpClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<CategoryProductsDto>> GetCategoryProductsAsync()
        {
            var response = await _client.GetAsync($"/api/v1/CategoryProduct");
            return await response.ReadContentAsAsync<IEnumerable<CategoryProductsDto>>();
        }
    }
}
