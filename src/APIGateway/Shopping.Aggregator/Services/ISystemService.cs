﻿using Shopping.Aggregator.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shopping.Aggregator.Services
{
    public interface ISystemService
    {
        Task<IEnumerable<CategoryProductsDto>> GetCategoryProductsAsync();
    }
}
