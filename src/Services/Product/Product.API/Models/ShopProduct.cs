﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

using System.Collections.Generic;

namespace Product.API.Models
{
    public class ShopProduct : BaseEntity
    {
        public ShopProduct()
        {
            CarouselImagePaths = new List<string>();
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement]
        public string CategoryId { get; set; }
        [BsonElement]
        public string Name { get; set; }
        [BsonElement]
        public decimal Price { get; set; }
        [BsonElement]
        public decimal Sale { get; set; }
        [BsonElement]
        public bool Status { get; set; }
        [BsonElement]
        public int TotalBought { get; set; }
        [BsonElement]
        public string ImageCardUrl { get; set; }
        [BsonElement]
        public string Summary { get; set; }
        [BsonElement]
        public string Description { get; set; }
        [BsonElement]
        public IEnumerable<string> CarouselImagePaths { get; set; }
    }
}
