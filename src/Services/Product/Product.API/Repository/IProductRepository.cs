﻿using Product.API.Helpers;
using Product.API.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Product.API.Repository
{
    public interface IProductRepository
    {
        Task CreateAsync(ShopProduct entity);
        Task<bool> UpdateAsync(ShopProduct entity);
        Task<bool> ChangeStatusAsync(ShopProduct entity);
        Task<PageList<ShopProduct>> SearchProductAsync(ProductSearchParams param);
        Task<PageList<ShopProduct>> SearchProductAdminAsync(ProductSearchAdminParams param);
        Task<IEnumerable<ShopProduct>> GetRelatedProductAsync(string id);
        Task<ShopProduct> GetDetailAsync(string id);
        Task<IEnumerable<ShopProduct>> GetManyByIds(IEnumerable<string> ids);
    }
}
