﻿using MongoDB.Driver;

using Product.API.Data;
using Product.API.Helpers;
using Product.API.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static Product.API.AppData.Enum;

namespace Product.API.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly IProductDbContext _context;

        public ProductRepository(IProductDbContext context)
        {
            _context = context;
        }

        public async Task<bool> ChangeStatusAsync(ShopProduct entity)
        {
            entity.Status = !entity.Status;

            var updateResult = await _context.Products.ReplaceOneAsync(x => entity.Id == x.Id, entity);
            return updateResult.IsAcknowledged && updateResult.ModifiedCount == 1;
        }

        public async Task CreateAsync(ShopProduct entity)
        {
            await _context.Products.InsertOneAsync(entity);
        }

        public async Task<ShopProduct> GetDetailAsync(string id)
        {
            return await _context.Products.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<ShopProduct>> GetManyByIds(IEnumerable<string> ids)
        {
            return await _context.Products
                .Find(Builders<ShopProduct>.Filter.In(x => x.Id, ids)).ToListAsync();
        }

        public async Task<IEnumerable<ShopProduct>> GetRelatedProductAsync(string id)
        {
            var currentProduct = await GetDetailAsync(id);
            return await _context.Products.Find(x => x.CategoryId == currentProduct.CategoryId && x.Id != id)
                .Limit(5).Skip(0)
                .ToListAsync();
        }

        public async Task<PageList<ShopProduct>> SearchProductAdminAsync(ProductSearchAdminParams param)
        {
            var builderFilter = Builders<ShopProduct>.Filter.Empty;

            if(!string.IsNullOrEmpty(param.Name))
            {
                builderFilter &= Builders<ShopProduct>.Filter.Where(x => x.Name.Contains(param.Name, StringComparison.OrdinalIgnoreCase));
            }

            if (!string.IsNullOrEmpty(param.CategoryId))
            {
                builderFilter &= Builders<ShopProduct>.Filter.Eq(x => x.CategoryId, param.CategoryId);
            }

            var findFluent = _context.Products.Find(builderFilter);
            return await PageList<ShopProduct>.CreateAsync(findFluent, param);
        }

        public async Task<PageList<ShopProduct>> SearchProductAsync(ProductSearchParams param)
        {
            var builderFilter = Builders<ShopProduct>.Filter.Empty;

            if (!string.IsNullOrEmpty(param.Name))
            {
                builderFilter &= Builders<ShopProduct>.Filter.Where(x => x.Name.Contains(param.Name, StringComparison.OrdinalIgnoreCase));
            }

            builderFilter &= Builders<ShopProduct>.Filter.In(x => x.CategoryId, param.CategoryIds);

            if (param.FromPrice.HasValue)
            {
                builderFilter &= Builders<ShopProduct>.Filter.Gte(x => x.Price - x.Sale, param.FromPrice.Value);
            }
            
            if (param.ToPrice.HasValue)
            {
                builderFilter &= Builders<ShopProduct>.Filter.Lte(x => x.Price - x.Sale, param.ToPrice.Value);
            }

            SortDefinition<ShopProduct> builderSort = null;
            if (param.OrderBy.HasValue)
            {
                switch (param.OrderBy.Value)
                {
                    case OrderBy.Price:
                        if (param.Direction.HasValue)
                        {
                            switch (param.Direction.Value)
                            {
                                case Direction.Asc:
                                    builderSort = Builders<ShopProduct>.Sort.Ascending(x => x.Price - x.Sale);
                                    break;
                                case Direction.Desc:
                                    builderSort = Builders<ShopProduct>.Sort.Descending(x => x.Price - x.Sale);
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                    case OrderBy.Popular:
                        if (param.Direction.HasValue)
                        {
                            switch (param.Direction.Value)
                            {
                                case Direction.Asc:
                                    builderSort = Builders<ShopProduct>.Sort.Ascending(x => x.TotalBought);
                                    break;
                                case Direction.Desc:
                                    builderSort = Builders<ShopProduct>.Sort.Descending(x => x.TotalBought);
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                    case OrderBy.New:
                        if (param.Direction.HasValue)
                        {
                            switch (param.Direction.Value)
                            {
                                case Direction.Asc:
                                    builderSort = Builders<ShopProduct>.Sort.Ascending(x => x.CreatedDate);
                                    break;
                                case Direction.Desc:
                                    builderSort = Builders<ShopProduct>.Sort.Descending(x => x.CreatedDate);
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            var findFluent = _context.Products.Find(builderFilter);
            if (builderSort != null)
            {
                findFluent = findFluent.Sort(builderSort);
            }
            return await PageList<ShopProduct>.CreateAsync(findFluent, param);
        }

        public async Task<bool> UpdateAsync(ShopProduct entity)
        {
            var updateResult = await _context.Products.ReplaceOneAsync(x => x.Id == entity.Id, entity);
            return updateResult.IsAcknowledged && updateResult.ModifiedCount == 1;
        }
    }
}
