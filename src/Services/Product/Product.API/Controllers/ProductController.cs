﻿using AutoMapper;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using MongoDB.Bson;

using Product.API.Dtos;
using Product.API.Helpers;
using Product.API.Models;
using Product.API.Repository;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Product.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepository _repository;
        private readonly ILogger<ProductController> _logger;
        private readonly IMapper _mapper;

        public ProductController(IProductRepository repository, ILogger<ProductController> logger, IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> CreateProduct(CreateProductDto productDto)
        {
            _logger.LogInformation("Create product");

            var productToCreate = _mapper.Map<ShopProduct>(productDto);
            productToCreate.Id = ObjectId.GenerateNewId().ToString();
            await _repository.CreateAsync(productToCreate);

            _logger.LogInformation("Create product success with id: " + productToCreate.Id);
            return CreatedAtRoute("GetDetail", new { id = productToCreate.Id }, productToCreate);
        }

        [HttpPost("ChangeStatus/{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ChangeStatus(string id)
        {
            _logger.LogInformation("Change status of product with id: " + id);
            var currentProduct = await _repository.GetDetailAsync(id);

            if (currentProduct == null)
            {
                _logger.LogError("Cannot found product");
                return NotFound();
            }
            else
            {
                var result = await _repository.ChangeStatusAsync(currentProduct);
                if (result)
                {
                    _logger.LogInformation("Change status successfully");
                    return Ok();
                }
                else
                {
                    _logger.LogInformation("There is an error when changing status of product");
                    return BadRequest();
                }
            }
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateProduct(UpdateProductDto productDto)
        {
            _logger.LogInformation("Update product with id: " + productDto.Id);

            // check product existed
            var currentProduct = await _repository.GetDetailAsync(productDto.Id);
            if (currentProduct == null)
            {
                _logger.LogError("Cannot found product");
                return NotFound();
            }
            else
            {
                var productToUpdate = _mapper.Map<ShopProduct>(productDto);

                productToUpdate.CreatedBy = currentProduct.CreatedBy;
                productToUpdate.CreatedDate = currentProduct.CreatedDate;
                productToUpdate.Status = currentProduct.Status;
                productToUpdate.CategoryId = currentProduct.CategoryId;

                var result = await _repository.UpdateAsync(productToUpdate);

                if (result)
                {
                    _logger.LogInformation("Update product success");
                    return Ok();
                }
                else
                {
                    _logger.LogError("Update product not success");
                    return BadRequest();
                }
            }
        }

        [HttpGet("Search")]
        [ProducesResponseType(typeof(ReturnListDto<ProductCardDto, ShopProduct>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ReturnListDto<ProductCardDto, ShopProduct>>> SearchProduct([FromQuery]ProductSearchParams @params)
        {
            _logger.LogInformation("Search product in customer page with params: " + JsonSerializer.Serialize(@params));
            var products = await _repository.SearchProductAsync(@params);

            return Ok(new ReturnListDto<ProductCardDto, ShopProduct>(products, _mapper));
        }

        [HttpGet("SearchAdmin")]
        [ProducesResponseType(typeof(ReturnListDto<ProductListDto, ShopProduct>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ReturnListDto<ProductListDto, ShopProduct>>> SearchProductAdmin(ProductSearchAdminParams @params)
        {
            _logger.LogInformation("Search product in admin page with params: " + JsonSerializer.Serialize(@params));
            var products = await _repository.SearchProductAdminAsync(@params);

            return Ok(new ReturnListDto<ProductListDto, ShopProduct>(products, _mapper));
        }

        [HttpGet("RelatedProducts/{id}")]
        [ProducesResponseType(typeof(IEnumerable<ProductCardDto>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<ProductCardDto>>> GetRelatedProducts(string id)
        {
            _logger.LogInformation("Get related products of id: " + id);
            var products = await _repository.GetRelatedProductAsync(id);

            return Ok(_mapper.Map<IEnumerable<ProductCardDto>>(products));
        }

        [HttpGet("{id}", Name = "GetDetail")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ProductDetailDto), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ProductDetailDto>> GetProductDetail(string id)
        {
            _logger.LogInformation("Get product detail with id: " + id);
            var product = await _repository.GetDetailAsync(id);

            if (product == null)
            {
                _logger.LogError("Cannot found product");
                return NotFound();
            }
            else
            {
                return Ok(_mapper.Map<ProductDetailDto>(product));
            }
        }

        [HttpPost("GetProductsCardByIds")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<ProductCardDto>>> GetProductsCardByIds(IEnumerable<string> ids)
        {
            _logger.LogInformation("Get products card with ids: " + JsonSerializer.Serialize(ids));

            var products = await _repository.GetManyByIds(ids);

            return Ok(_mapper.Map<IEnumerable<ProductCardDto>>(products).ToList());

        }
        [HttpPost("GetProductsByIds")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<ProductCardDto>>> GetProductsByIds(IEnumerable<string> ids)
        {
            _logger.LogInformation("Get products with ids: " + JsonSerializer.Serialize(ids));

            var products = await _repository.GetManyByIds(ids);

            return Ok(_mapper.Map<IEnumerable<ProductDto>>(products).ToList());

        }

    }
}
