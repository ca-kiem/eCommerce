﻿
using System.Collections.Generic;

using static Product.API.AppData.Enum;

namespace Product.API.Helpers
{
    public class ProductSearchParams : BaseParams
    {
        public ProductSearchParams()
        {
            CategoryIds = new List<string>();
        }
        public IEnumerable<string> CategoryIds { get; set; }
        public string Name { get; set; }
        public decimal? FromPrice { get; set; }
        public decimal? ToPrice { get; set; }
        public OrderBy? OrderBy { get; set; }
        public Direction? Direction { get; set; }
    }
}
