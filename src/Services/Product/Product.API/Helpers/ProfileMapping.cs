﻿using AutoMapper;

using Microsoft.Extensions.Configuration;

using Product.API.Dtos;
using Product.API.Models;

using System.Collections.Generic;
using System.IO;

namespace Product.API.Helpers
{
    public class ProfileMapping : Profile
    {
        public ProfileMapping(IConfiguration configuration)
        {
            CreateMap<CreateProductDto, ShopProduct>();
            CreateMap<UpdateProductDto, ShopProduct>();
            CreateMap<ShopProduct, ProductDto>()
                .ForMember(x => x.ImageFile, x => x.MapFrom(y => string.Join(configuration.GetValue<string>("StoragePaths:Image"), Path.DirectorySeparatorChar, y.ImageCardUrl)));
            CreateMap<ShopProduct, ProductCardDto>()
                .ForMember(x => x.ImageCardUrl, x => x.MapFrom(y => string.Join(configuration.GetValue<string>("StoragePaths:Image"), Path.DirectorySeparatorChar, y.ImageCardUrl)));
            CreateMap<ShopProduct, ProductListDto>();
            CreateMap<ShopProduct, ProductDetailDto>()
                .ForMember(x => x.CarouselImagePaths, x => x.MapFrom(y => SetCarouselImagePath(y.CarouselImagePaths, configuration.GetValue<string>("StoragePaths:Image"))));
        }

        private static IEnumerable<string> SetCarouselImagePath(IEnumerable<string> carouselImagePaths, string basePath)
        {
            var resultListPaths = new List<string>();
            foreach (var path in carouselImagePaths)
            {
                resultListPaths.Add(string.Join(basePath, Path.DirectorySeparatorChar, path));
            }

            return resultListPaths;
        }
    }
}
