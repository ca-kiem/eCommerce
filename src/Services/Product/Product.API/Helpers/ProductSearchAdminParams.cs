﻿namespace Product.API.Helpers
{
    public class ProductSearchAdminParams : BaseParams
    {
        public string CategoryId { get; set; }
        public string Name { get; set; }
    }
}
