﻿using MongoDB.Driver;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Product.API.Helpers
{
    public class PageList<T> : List<T> where T : class
    {
        public PageList(IList<T> items, int pageSize, int currentPage, long totalItems)
        {
            this.PageSize = pageSize;
            this.CurrentPage = currentPage;
            this.TotalItems = totalItems;
            this.TotalPages = (int)Math.Ceiling(totalItems / (double)pageSize);

            this.AddRange(items);
        }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public long TotalItems { get; set; }
        public int TotalPages { get; set; }

        public static async Task<PageList<T>> CreateAsync(IFindFluent<T, T> queryable, BaseParams paramsFilter)
        {
            var totalCounts = await queryable.CountDocumentsAsync();

            var totalPages = (int)Math.Ceiling(totalCounts / (double)paramsFilter.PageSize);
            paramsFilter.CurrentPage = paramsFilter.CurrentPage > totalPages ? totalPages : paramsFilter.CurrentPage;
            paramsFilter.CurrentPage = paramsFilter.CurrentPage == 0 ? 1 : paramsFilter.CurrentPage;

            var items = await queryable.Skip((paramsFilter.CurrentPage - 1) * paramsFilter.PageSize).Limit(paramsFilter.PageSize).ToListAsync();
            return new PageList<T>(items, paramsFilter.PageSize, paramsFilter.CurrentPage, totalCounts);
        }

    }
}