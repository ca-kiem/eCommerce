﻿using Microsoft.Extensions.DependencyInjection;

using Product.API.Data;
using Product.API.Repository;

namespace Product.API.Helpers
{
    public static class DIHelper
    {
        internal static IServiceCollection InitDI(this IServiceCollection services)
        {
            services.AddScoped<IProductDbContext, ProductDbContext>();
            services.AddScoped<IProductRepository, ProductRepository>();
            return services;
        }
    }
}
