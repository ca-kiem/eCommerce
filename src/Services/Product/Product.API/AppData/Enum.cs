﻿namespace Product.API.AppData
{
    public class Enum
    {
        public enum OrderBy
        {
            Price,
            Popular,
            New
        }

        public enum Direction
        {
            Asc,
            Desc
        }
    }
}
