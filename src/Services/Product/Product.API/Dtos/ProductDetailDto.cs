﻿using System.Collections.Generic;

namespace Product.API.Dtos
{
    public class ProductDetailDto
    {
        public ProductDetailDto()
        {
            CarouselImagePaths = new List<string>();
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal Sale { get; set; }
        public string CategoryId { get; set; }
        public string Description { get; set; }
        public string Summary { get; set; }
        public bool Status { get; set; }
        public IEnumerable<string> CarouselImagePaths { get; set; }
    }
}
