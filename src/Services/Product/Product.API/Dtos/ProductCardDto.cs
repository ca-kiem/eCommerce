﻿namespace Product.API.Dtos
{
    public class ProductCardDto
    {
        public string Id { get; set; }
        public string ImageCardUrl { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public decimal Price { get; set; }
        public decimal Sale { get; set; }
        public string Summary { get; set; }
    }
}
