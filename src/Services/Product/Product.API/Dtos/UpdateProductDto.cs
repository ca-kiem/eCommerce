﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Product.API.Dtos
{
    public class UpdateProductDto
    {
        public UpdateProductDto()
        {
            CarouselImagePaths = new List<string>();
        }
        [Required]
        public string Id { get; set; }
        [Required]
        public string ImageCardUrl { get; set; }
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal Sale { get; set; }
        [Required]
        public string CategoryId { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Summary { get; set; }
        public IEnumerable<string> CarouselImagePaths { get; set; }
    }
}
