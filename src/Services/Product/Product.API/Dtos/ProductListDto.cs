﻿using System;

namespace Product.API.Dtos
{
    public class ProductListDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal Sale { get; set; }

        /// <summary>
        /// If there isn't have any update, take createdBy value
        /// </summary>
        public string LastModifiedBy { get; set; }
        /// <summary>
        /// If there isn't have any update, take createdDate value
        /// </summary>
        public DateTime LastModifiedDate { get; set; }
    }
}
