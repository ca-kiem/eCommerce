﻿using AutoMapper;

using Product.API.Helpers;

using System.Collections.Generic;

namespace Product.API.Dtos
{
    public class ReturnListDto<TDestination, TSource> where TSource : class
    {
        public ReturnListDto(PageList<TSource> page, IMapper _mapper) 
        {
            Data = _mapper.Map<IEnumerable<TDestination>>(page);

            PageSize = page.PageSize;
            CurrentPage = page.CurrentPage;
            TotalItems = page.TotalItems;
            TotalPages = page.TotalPages;
        }

        public ReturnListDto()
        {
            Data = new List<TDestination>();
        }
        public IEnumerable<TDestination> Data { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public long TotalItems { get; set; }
        public int TotalPages { get; set; }
    }
}
