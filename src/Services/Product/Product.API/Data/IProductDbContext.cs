﻿using MongoDB.Driver;

using Product.API.Models;

namespace Product.API.Data
{
    public interface IProductDbContext
    {
        IMongoCollection<ShopProduct> Products { get; }
    }
}
