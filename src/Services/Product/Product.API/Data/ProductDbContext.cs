﻿using Microsoft.Extensions.Configuration;

using MongoDB.Driver;

using Product.API.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product.API.Data
{
    public class ProductDbContext : IProductDbContext
    {
        public IMongoCollection<ShopProduct> Products { get; }

        public ProductDbContext(IConfiguration configuration)
        {
            var client = new MongoClient(configuration.GetValue<string>("DatabaseSettings:ConnectionString"));
            var database = client.GetDatabase(configuration.GetValue<string>("DatabaseSettings:DatabaseName"));

            Products = database.GetCollection<ShopProduct>(configuration.GetValue<string>("DatabaseSettings:CollectionNames:Product"));
            
        }
    }
}
