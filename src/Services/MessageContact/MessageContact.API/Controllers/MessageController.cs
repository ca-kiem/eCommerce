﻿using AutoMapper;

using MessageContact.API.Dtos;
using MessageContact.API.Helpers;
using MessageContact.API.Models;
using MessageContact.API.Repository;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace MessageContact.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class MessageController : ControllerBase
    {
        private readonly IMessageRepository _repository;
        private readonly ILogger<MessageController> _logger;
        private readonly IMapper _mapper;

        public MessageController(IMessageRepository repository, ILogger<MessageController> logger, IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet("Search", Name = "SearchMessage")]
        [ProducesResponseType(typeof(ReturnListDto<MessageListDto, Message>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ReturnListDto<MessageListDto, Message>>> SearchMessage([FromQuery] MessageSearchParams @params)
        {
            _logger.LogInformation("Search Message in admin page with param: " + JsonSerializer.Serialize(@params));

            var Messages = await _repository.SearchMessagesAsync(@params);

            return new ReturnListDto<MessageListDto, Message>(Messages, _mapper);
        }

        [HttpGet("{id}", Name = "GetMessage")]
        [ProducesResponseType(typeof(MessageDetailDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<MessageDetailDto>> GetMessageDetail(int id)
        {
            _logger.LogInformation("Get detail message with id " + id);
            var message = await _repository.GetDetailAsync(id);

            if (message == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(_mapper.Map<MessageDetailDto>(message));
            }
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> CreateMessage([FromBody] CreateMessageDto messageDto)
        {
            _logger.LogInformation("Create message");
            var messageToCreate = _mapper.Map<Message>(messageDto);
            var createdMessage = await _repository.CreateAsync(messageToCreate);

            return CreatedAtRoute("GetMessage", new { id = createdMessage.Id }, createdMessage);
        }

        [HttpPost("ChangeStatus/{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> ChangeStatus(int id)
        {
            _logger.LogInformation("Change status of message with id: " + id);
            var currentMessage = await _repository.GetDetailAsync(id);
            if (currentMessage == null)
            {
                return NotFound();
            }
            else
            {
                await _repository.ChangeStatusAsync(currentMessage);
                return Ok();
            }
        }
    }
}
