﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MessageContact.API.Models
{
    public class Message
    {
        public Message()
        {
            CreatedDate = DateTime.Now;
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        [Required]
        [MaxLength(15)]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        public string PhoneNumber { get; set; }

        [Required]
        [MaxLength(255)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MaxLength(1023)]
        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Read status
        /// false: unread
        /// true: read
        /// </summary>
        public bool ReadStatus { get; set; }
    }
}
