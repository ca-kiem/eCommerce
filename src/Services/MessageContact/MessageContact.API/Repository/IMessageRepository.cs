﻿using MessageContact.API.Helpers;
using MessageContact.API.Models;

using System.Threading.Tasks;

namespace MessageContact.API.Repository
{
    public interface IMessageRepository
    {
        Task<PageList<Message>> SearchMessagesAsync(MessageSearchParams param);
        Task<Message> GetDetailAsync(int messageId);
        Task<Message> CreateAsync(Message message);
        Task ChangeStatusAsync(Message message);
    }
}
