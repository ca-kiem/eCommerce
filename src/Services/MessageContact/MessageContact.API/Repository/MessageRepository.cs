﻿using MessageContact.API.Data;
using MessageContact.API.Helpers;
using MessageContact.API.Models;

using Microsoft.EntityFrameworkCore;

using System;
using System.Linq;
using System.Threading.Tasks;

namespace MessageContact.API.Repository
{
    public class MessageRepository : IMessageRepository
    {
        private readonly MessageDbContext _context;

        public MessageRepository(MessageDbContext context)
        {
            _context = context;
        }

        public async Task ChangeStatusAsync(Message message)
        {
            message.ReadStatus = !message.ReadStatus;
            _context.Entry(message).State = EntityState.Modified;
            _context.Messages.Update(message);
            await _context.SaveChangesAsync();
        }

        public async Task<Message> CreateAsync(Message message)
        {
            await _context.Messages.AddAsync(message);

            await _context.SaveChangesAsync();
            return message;
        }

        public async Task<Message> GetDetailAsync(int messageId)
        {
            return await _context.Messages.FindAsync(messageId);
        }

        public async Task<PageList<Message>> SearchMessagesAsync(MessageSearchParams param)
        {
            var query = _context.Messages.AsQueryable();
            if (!string.IsNullOrEmpty(param.Content))
            {
                query = query.Where(x => x.Content.Contains(param.Content, StringComparison.OrdinalIgnoreCase));
            }

            if (!string.IsNullOrEmpty(param.Email))
            {
                query = query.Where(x => x.Email.Contains(param.Email, StringComparison.OrdinalIgnoreCase));
            }
            if (!string.IsNullOrEmpty(param.Name))
            {
                query = query.Where(x => x.Name.Contains(param.Name, StringComparison.OrdinalIgnoreCase));
            }
            if (!string.IsNullOrEmpty(param.PhoneNumber))
            {
                query = query.Where(x => x.PhoneNumber.Contains(param.PhoneNumber, StringComparison.OrdinalIgnoreCase));
            }

            if (param.Status.HasValue)
            {
                query = query.Where(x => x.ReadStatus == param.Status.Value);
            }

            query.OrderByDescending(x => x.CreatedDate);

            return await PageList<Message>.CreateAsync(query, param);
        }
    }
}
