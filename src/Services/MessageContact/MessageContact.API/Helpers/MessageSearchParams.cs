﻿
using System.Collections.Generic;

namespace MessageContact.API.Helpers
{
    public class MessageSearchParams : BaseParams
    {
        public string Content { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public bool? Status { get; set; }
    }
}
