﻿namespace MessageContact.API.Helpers
{
    public class BaseParams
    {
        private const int MAX_PAGE_SIZE = 50;
        public int CurrentPage { get; set; } = 1;   // defalt in first page
        private int pageSize = 25;  // default has 24 items per page 
        public int PageSize
        {
            get { return pageSize; }
            set { pageSize = value > MAX_PAGE_SIZE ? MAX_PAGE_SIZE : value; }
        }
    }
}
