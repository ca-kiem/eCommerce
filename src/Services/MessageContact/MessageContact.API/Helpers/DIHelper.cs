﻿using MessageContact.API.Repository;

using Microsoft.Extensions.DependencyInjection;


namespace MessageContact.API.Helpers
{
    public static class DIHelper
    {
        internal static IServiceCollection InitDI(this IServiceCollection services)
        {
            services.AddScoped<IMessageRepository, MessageRepository>();

            return services;
        }
    }
}
