﻿using AutoMapper;

using MessageContact.API.Dtos;
using MessageContact.API.Models;

using System;

namespace MessageContact.API.Helpers
{
    public class ProfileMapping : Profile
    {
        public ProfileMapping()
        {
            CreateMap<CreateMessageDto, Message>()
                .ForMember(x => x.CreatedDate, x => x.MapFrom(y => DateTime.Now))
                .ForMember(x => x.Content, x => x.MapFrom(y => y.Message))
                .ForMember(x => x.ReadStatus, x => x.MapFrom(y => false));

            CreateMap<Message, MessageListDto>();
            CreateMap<Message, MessageDetailDto>();
        }
    }
}
