﻿using AutoMapper;

using MediatR;

using Microsoft.Extensions.Logging;

using Order.Application.Contracts.Persistence;
using Order.Application.Exceptions;

using System.Threading;
using System.Threading.Tasks;

namespace Order.Application.Features.Orders.Command.UpdateOrder
{
    public class UpdateOrderCommandHandler : IRequestHandler<UpdateOrderCommand>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly ILogger<UpdateOrderCommandHandler> _logger;
        private readonly IMapper _mapper;

        public UpdateOrderCommandHandler(IOrderRepository orderRepository, ILogger<UpdateOrderCommandHandler> logger, IMapper mapper)
        {
            _orderRepository = orderRepository;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateOrderCommand request, CancellationToken cancellationToken)
        {
            var currentOrder = await _orderRepository.GetByIdAsync(request.Id);

            if (currentOrder == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Order), request.Id);
            }
            else
            {
                _mapper.Map(request, currentOrder, typeof(UpdateOrderCommand), typeof(Domain.Entities.Order));
                await _orderRepository.UpdateAsync(currentOrder);
                _logger.LogWarning("Order has updateed with id: " + request.Id);
            }

            return Unit.Value;
        }
    }
}
