﻿using FluentValidation;

namespace Order.Application.Features.Orders.Command.UpdateOrder
{
    public class UpdateOrderCommandValidator : AbstractValidator<UpdateOrderCommand>
    {
        public UpdateOrderCommandValidator()
        {
            RuleFor(x => x.BuyerEmailAddress).NotEmpty()
                .WithMessage("{EmailAdress} is required.")
                .MaximumLength(255).WithMessage("{EmailAddress} should not exceeed 255 characters");
            RuleFor(x => x.BuyerPhoneNumber).NotEmpty()
                .WithMessage("{PhoneNumber} is required.")
                .MaximumLength(15).WithMessage("{PhoneNumber} not correct");
            RuleFor(x => x.BuyerFirstName).NotEmpty()
                .WithMessage("{FirstName} is required.")
                .MaximumLength(50).WithMessage("{FirstName} should not exceeed 50 characters");
        }
    }
}
