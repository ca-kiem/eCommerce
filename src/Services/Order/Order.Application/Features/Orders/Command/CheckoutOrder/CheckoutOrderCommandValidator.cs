﻿using FluentValidation;

namespace Order.Application.Features.Orders.Command.CheckoutOrder
{
    public class CheckoutOrderCommandValidator : AbstractValidator<CheckoutOrderCommand>
    {
        public CheckoutOrderCommandValidator()
        {
            RuleFor(x => x.BuyerEmailAddress).NotEmpty()
                .WithMessage("{EmailAdress} is required.")
                .MaximumLength(50).WithMessage("{EmailAddress} should not exceeed 50 characters");
            RuleFor(x => x.BuyerPhoneNumber).NotEmpty()
                .WithMessage("{PhoneNumber} is required.")
                .MaximumLength(15).WithMessage("{PhoneNumber} not correct");
            RuleFor(x => x.BuyerFirstName).NotEmpty()
                .WithMessage("{FirstName} is required.")
                .MaximumLength(50).WithMessage("{FirstName} should not exceeed 50 characters");
        }
    }
}
