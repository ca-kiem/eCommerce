﻿namespace Order.Application.Features.Orders.Command.CheckoutOrder
{
    public class ShoppingCartItem
    {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public uint Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
