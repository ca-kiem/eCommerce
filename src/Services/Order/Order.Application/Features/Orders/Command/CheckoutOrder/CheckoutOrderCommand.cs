﻿using MediatR;

using System;
using System.Collections.Generic;

namespace Order.Application.Features.Orders.Command.CheckoutOrder
{
    public class CheckoutOrderCommand : IRequest<string>
    {
        public CheckoutOrderCommand()
        {
            Items = new List<ShoppingCartItem>();
        }
        public string UserId { get; set; }
        public decimal TotalPrice { get; set; }

        // BillingAddress
        public string BuyerFirstName { get; set; }
        public string BuyerLastName { get; set; }
        public string BuyerPhoneNumber { get; set; }

        public string BuyerEmailAddress { get; set; }
        public string BuyerAddressLine { get; set; }
        public string BuyerState { get; set; }

        // shipping information
        public string ReceiverFirstName { get; set; }
        public string ReceiverLastName { get; set; }
        public string ReceiverPhoneNumber { get; set; }
        public string ReceiverEmailAddress { get; set; }
        public string ReceiverAddressLine { get; set; }
        public string ReceiverState { get; set; }

        public string Note { get; set; }
        // Payment
        public int PaymentMethod { get; set; }

        /// <summary>
        /// Is shipping address same as buyer address
        /// </summary>
        public bool ShippingDifferentAddress { get; set; } = false;
        /// <summary>
        /// Status of the order
        /// <para>0: Not process</para>
        /// <para>1: Processing</para>
        /// <para>2: Done</para>
        /// <para>3: Cancel</para>
        /// </summary>
        public int Status { get; set; }

        public IEnumerable<ShoppingCartItem> Items { get; set; }
    }
}
