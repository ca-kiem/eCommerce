﻿using AutoMapper;

using MediatR;

using Microsoft.Extensions.Logging;

using Order.Application.Contracts.Infrastructure;
using Order.Application.Contracts.Persistence;
using Order.Application.Models;

using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Order.Application.Features.Orders.Command.CheckoutOrder
{
    public class CheckoutOrderCommandHandler : IRequestHandler<CheckoutOrderCommand, string>
    {
        private readonly IOrderRepository _repository;
        private readonly IMapper _mapper;
        private readonly IEmailService _emailService;
        private readonly ILogger<CheckoutOrderCommandHandler> _logger;

        public CheckoutOrderCommandHandler(IOrderRepository repository, IMapper mapper, IEmailService emailService, ILogger<CheckoutOrderCommandHandler> logger)
        {
            _repository = repository;
            _mapper = mapper;
            _emailService = emailService;
            _logger = logger;
        }

        public async Task<string> Handle(CheckoutOrderCommand request, CancellationToken cancellationToken)
        {
            var orderEntity = _mapper.Map<Domain.Entities.Order>(request);

            var newOrder = await _repository.AddAsync(orderEntity);
            _logger.LogInformation("Create order successfully with value: " + JsonSerializer.Serialize(newOrder));

            // send email
            await SendEmail(newOrder);

            return newOrder.Id;
        }

        private async Task SendEmail(Domain.Entities.Order newOrder)
        {
            // send email to customer
            var emailContentCustomer = new Email
            {
                To = newOrder.BuyerEmailAddress,
                Title = "Email title",
                Body = "Email body"
            };

            // send email to seller
            var emailContentSeller = new Email
            {
                // TODO: Get email of the sellers
                To = "",
                Title = "Email title seller",
                Body = "Email body seller"
            };

            try
            {
                await _emailService.SendEmailAsync(emailContentSeller);
                await _emailService.SendEmailAsync(emailContentCustomer);
            }
            catch (Exception e)
            {
                _logger.LogError("Failed to send email services.", e);
            }
        }
    }
}
