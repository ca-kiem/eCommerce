﻿namespace Order.Application.Features.Orders.Queries.GetOrderList
{
    public class OrdersVm
    {
        public string Id { get; set; }
        public string CreatedDate { get; set; }
        public decimal Total { get; set; }
        public int ProductQuantities { get; set; }
        public int Status { get; set; }
    }
}