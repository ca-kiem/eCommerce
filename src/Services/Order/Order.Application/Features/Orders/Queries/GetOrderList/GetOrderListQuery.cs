﻿using MediatR;

using Order.Domain.Helpers;

namespace Order.Application.Features.Orders.Queries.GetOrderList
{
    public class GetOrderListQuery : IRequest<ReturnListDto<OrdersVm, Domain.Entities.Order>>
    {
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string OrderId { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }

        public GetOrderListQuery(string orderId, string phoneNumber, string name, string emailAddress, int currentPage, int pageSize)
        {
            OrderId = orderId;
            PhoneNumber = phoneNumber;
            Name = name;
            EmailAddress = emailAddress;
            CurrentPage = currentPage;
            PageSize = pageSize;
        }
    }
}
