﻿using AutoMapper;

using MediatR;

using Order.Application.Contracts.Persistence;
using Order.Domain.Helpers;

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Order.Application.Features.Orders.Queries.GetOrderList
{
    public class GetOrderListQueryHandler : IRequestHandler<GetOrderListQuery, ReturnListDto<OrdersVm, Domain.Entities.Order>>
    {
        private readonly IOrderRepository _repository;
        private readonly IMapper _mapper;

        public GetOrderListQueryHandler(IOrderRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<ReturnListDto<OrdersVm, Domain.Entities.Order>> Handle(GetOrderListQuery request, CancellationToken cancellationToken)
        {

            var orders = await _repository.GetOrdersAsync(_mapper.Map<GetOrderListParams>(request));

            return new ReturnListDto<OrdersVm, Domain.Entities.Order>(orders, _mapper.Map<IEnumerable<OrdersVm>>(orders.AsEnumerable()));
        }
    }
}
