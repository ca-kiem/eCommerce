﻿using FluentValidation.Results;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Order.Application.Exceptions
{
    public class ValidatorException : ApplicationException
    {
        public ValidatorException()
            : base("One or more validation failure have occurred")
        {
            Errors = new Dictionary<string, string[]>();
        }

        public ValidatorException(IEnumerable<ValidationFailure> failures)
            : this()
        {
            Errors = failures.GroupBy(x => x.PropertyName, e => e.ErrorMessage)
                .ToDictionary(x => x.Key, x => x.ToArray());
        }

        public IDictionary<string, string[]> Errors { get; }

    }
}
