﻿using AutoMapper;

using Order.Application.Features.Orders.Command.CheckoutOrder;
using Order.Application.Features.Orders.Command.UpdateOrder;
using Order.Application.Features.Orders.Queries.GetOrderList;
using Order.Domain.Entities;
using Order.Domain.Helpers;

namespace Order.Application.Helpers
{
    public class ProfileMapping : Profile
    {
        public ProfileMapping()
        {
            CreateMap<ShoppingCartItem, OrderDetail>();

            CreateMap<OrdersVm, Domain.Entities.Order>().ReverseMap();
            CreateMap<CheckoutOrderCommand, Domain.Entities.Order>();
            CreateMap<UpdateOrderCommand, Domain.Entities.Order>();

            CreateMap<GetOrderListQuery, GetOrderListParams>();
        }
    }
}
