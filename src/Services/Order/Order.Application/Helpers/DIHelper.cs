﻿using FluentValidation;

using MediatR;

using Microsoft.Extensions.DependencyInjection;

using Order.Application.Behaviors;

using System.Reflection;

namespace Order.Application.Helpers
{
    public class DIHelper
    {
        public static void Init(IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            services.AddMediatR(Assembly.GetExecutingAssembly());

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
        }
    }
}
