﻿namespace Order.Application.Models
{
    public class Email
    {
        public string To { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
