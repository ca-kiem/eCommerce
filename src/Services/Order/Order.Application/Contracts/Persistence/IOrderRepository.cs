﻿using Order.Domain.Helpers;

using System.Threading.Tasks;

namespace Order.Application.Contracts.Persistence
{
    public interface IOrderRepository : IAsyncRepository<Domain.Entities.Order>
    {
        Task<PageList<Domain.Entities.Order>> GetOrdersAsync(GetOrderListParams @params);
    }
}
