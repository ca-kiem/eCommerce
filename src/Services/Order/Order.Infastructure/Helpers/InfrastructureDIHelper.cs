﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Order.Application.Contracts.Infrastructure;
using Order.Application.Contracts.Persistence;
using Order.Application.Models;
using Order.Infastructure.Persistence;
using Order.Infastructure.Repositories;
using Order.Infastructure.Services;

namespace Order.Infastructure.Helpers
{
    public class InfrastructureDIHelper
    {
        public static void Init(IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("OrderAPIMysql");
            var serverVersion = ServerVersion.AutoDetect(connectionString);

            services.AddDbContext<OrderContext>(options =>
            {
                options.UseMySql(connectionString, serverVersion)
                .EnableSensitiveDataLogging() // These two calls are optional but help
                    .EnableDetailedErrors();      // with debugging (remove for production).
            });

            services.AddScoped(typeof(IAsyncRepository<>), typeof(RepositoryBase<>));
            services.AddScoped<IOrderRepository, OrderRepository>();

            services.Configure<EmailSetting>(x => configuration.GetSection("EmailSettings"));
            services.AddTransient<IEmailService, EmailService>();
        }
    }
}
