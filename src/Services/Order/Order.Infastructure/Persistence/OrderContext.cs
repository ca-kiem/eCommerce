﻿using Microsoft.EntityFrameworkCore;

using Order.Domain.Common;
using Order.Domain.Entities;

using System;
using System.Threading;
using System.Threading.Tasks;

namespace Order.Infastructure.Persistence
{
    public class OrderContext : DbContext
    {
        public OrderContext(DbContextOptions<OrderContext> options) : base(options)
        {
        }

        public DbSet<Domain.Entities.Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<OrderDetail>(config =>
            {
                config.HasKey(x => new { x.OrderId, x.ProductId });
            });
            
            modelBuilder.Entity<Domain.Entities.Order>(config =>
            {
                config.HasKey(x => x.Id);
                
                config.Property(x => x.Id)
                .HasComment("Order id with format: ORD{yyyyMMdd}{indexOrderInday:D4}")
                .HasMaxLength(15);
            });
        }
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            foreach (var entry in ChangeTracker.Entries<EntityBase>())
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.Entity.LastModifiedBy = "";
                        entry.Entity.LastModifiedDate = DateTime.Now;
                        break;
                    case EntityState.Added:
                        entry.Entity.CreatedBy = "";
                        entry.Entity.CreatedDate = DateTime.Now;
                        break;
                    case EntityState.Detached:
                    case EntityState.Unchanged:
                    case EntityState.Deleted:
                    default:
                        break;
                }
            }
            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}
