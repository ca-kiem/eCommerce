﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Order.Application.Contracts.Infrastructure;
using Order.Application.Models;

using SendGrid;
using SendGrid.Helpers.Mail;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order.Infastructure.Services
{
    public class EmailService : IEmailService
    {
        public EmailSetting _emailSetting { get; }
        public ILogger<EmailService> _logger { get; }

        public EmailService(IOptions<EmailSetting> emailSetting, ILogger<EmailService> logger)
        {
            _emailSetting = emailSetting.Value;
            _logger = logger;
        }

        public async Task<bool> SendEmailAsync(Email email)
        {
            var client = new SendGridClient(_emailSetting.ApiKey);
            var from = new EmailAddress
            {
                Email = _emailSetting.FromAddress,
                Name = _emailSetting.FromName
            };
            var to = new EmailAddress(email.To);
            var sendgridMessage = MailHelper.CreateSingleEmail(from, to, email.Title, email.Body, email.Body);
            var response = await client.SendEmailAsync(sendgridMessage);
            if (response.IsSuccessStatusCode)
            {
                _logger.LogInformation("Send email success");
                return true;
            }
            else
            {
                _logger.LogInformation("Send email not success");
                return false;
            }

        }
    }
}
