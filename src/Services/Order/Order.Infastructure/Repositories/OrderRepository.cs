﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using Order.Application.Contracts.Persistence;
using Order.Domain.Entities;
using Order.Domain.Helpers;
using Order.Infastructure.Persistence;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order.Infastructure.Repositories
{
    public class OrderRepository : RepositoryBase<Domain.Entities.Order>, IOrderRepository
    {
        public OrderRepository(OrderContext context) : base(context)
        {
        }

        public async Task<PageList<Domain.Entities.Order>> GetOrdersAsync(GetOrderListParams @params)
        {
            var query = _context.Orders.AsQueryable();
            if (!string.IsNullOrEmpty(@params.EmailAddress))
            {
                query = query.Where(x => x.BuyerEmailAddress.Contains(@params.EmailAddress, StringComparison.OrdinalIgnoreCase));
            }

            if (!string.IsNullOrEmpty(@params.Name))
            {
                query = query.Where(x => x.BuyerFirstName.Contains(@params.Name, StringComparison.OrdinalIgnoreCase));
            }

            if (!string.IsNullOrEmpty(@params.PhoneNumber))
            {
                query = query.Where(x => x.BuyerPhoneNumber.Contains(@params.PhoneNumber, StringComparison.OrdinalIgnoreCase));
            }
            
            if (!string.IsNullOrEmpty(@params.OrderId))
            {
                query = query.Where(x => x.Id.Contains(@params.OrderId, StringComparison.OrdinalIgnoreCase));
            }

            query = query.OrderByDescending(x => x.CreatedDate);

            return await PageList<Domain.Entities.Order>.CreateAsync(query, @params);
        }
    }
}
