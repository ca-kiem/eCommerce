﻿using MediatR;

using Microsoft.AspNetCore.Mvc;

using Order.Application.Features.Orders.Command.CheckoutOrder;
using Order.Application.Features.Orders.Command.UpdateOrder;

using System.Net;
using System.Threading.Tasks;

namespace Order.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OrderController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost(Name = "CheckoutOrder")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> CheckoutOrder([FromBody] CheckoutOrderCommand orderCommand)
        {
            var result = await _mediator.Send(orderCommand);
            return Ok(result);
        }

        [HttpPut(Name = "UpdateOrder")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<int>> UpdateOrder([FromBody] UpdateOrderCommand orderCommand)
        {
            await _mediator.Send(orderCommand);
            return NoContent();
        }

    }
}
