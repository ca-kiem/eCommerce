﻿using AutoMapper;

using EventBus.Messages.Events;

using Order.Application.Features.Orders.Command.CheckoutOrder;

namespace Order.API.Helpers
{
    public class ProfileMapping : Profile
    {
        public ProfileMapping()
        {
            CreateMap<CheckoutOrderCommand, BasketCheckoutEvent>().ReverseMap();
        }
    }
}
