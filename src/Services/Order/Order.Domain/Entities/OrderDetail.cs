﻿using System.ComponentModel.DataAnnotations;

namespace Order.Domain.Entities
{
    public class OrderDetail
    {
        [MaxLength(15)]
        public string OrderId { get; set; }
        [MaxLength(24)]
        public string ProductId { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }

        public virtual Order BaseOrder { get; set; }
    }
}
