﻿namespace Order.Domain.Helpers
{
    public class GetOrderListParams : BaseParams
    {
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string OrderId { get; set; }
    }
}
