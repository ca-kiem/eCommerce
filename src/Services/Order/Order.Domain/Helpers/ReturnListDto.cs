﻿using System.Collections.Generic;

namespace Order.Domain.Helpers
{
    public class ReturnListDto<TDestination, TSource> where TSource : class
    {
        public ReturnListDto(PageList<TSource> page, IEnumerable<TDestination> data) 
        {
            Data = data;

            PageSize = page.PageSize;
            CurrentPage = page.CurrentPage;
            TotalItems = page.TotalItems;
            TotalPages = page.TotalPages;
        }

        public ReturnListDto()
        {
            Data = new List<TDestination>();
        }
        public IEnumerable<TDestination> Data { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages { get; set; }
    }
}
