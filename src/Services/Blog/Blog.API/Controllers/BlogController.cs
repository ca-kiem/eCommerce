﻿using AutoMapper;

using Blog.API.Dtos;
using Blog.API.Helpers;
using Blog.API.Models;
using Blog.API.Repository.Interfaces;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace Blog.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class BlogController : ControllerBase
    {
        private readonly IBlogRepository _repository;
        private readonly ILogger<BlogController> _logger;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public BlogController(IBlogRepository repository, ILogger<BlogController> logger, IMapper mapper, IConfiguration configuration)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
            _configuration = configuration;
        }

        [HttpGet("Search", Name = "SearchBlog")]
        [ProducesResponseType(typeof(ReturnListDto<BlogCardDto, AdminBlog>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ReturnListDto<BlogCardDto, AdminBlog>>> SearchBlog([FromQuery] BlogSearchParams @params)
        {
            _logger.LogInformation("Search blog in customer page with param: " + JsonSerializer.Serialize(@params));

            var blogs = await _repository.SearchBlogsAsync(@params);

            return new ReturnListDto<BlogCardDto, AdminBlog>(blogs, _mapper);
        }

        [HttpGet("SearchAdmin", Name = "SearchBlogAdmin")]
        [ProducesResponseType(typeof(ReturnListDto<BlogListDto, AdminBlog>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ReturnListDto<BlogListDto, AdminBlog>>> SearchBlogAdmin(BlogSearchParams @params)
        {
            _logger.LogInformation("Search blog in admin page with param: " + JsonSerializer.Serialize(@params));
            var blogs = await _repository.SearchBlogsAsync(@params);

            return new ReturnListDto<BlogListDto, AdminBlog>(blogs, _mapper);
        }
        [HttpGet("GetLastest", Name = "GetLastest")]
        [ProducesResponseType(typeof(IEnumerable<BlogCardDto>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<BlogCardDto>>> GetLastest()
        {
            _logger.LogInformation("Get lastest blogs");
            var blogs = await _repository.GetLastestAsync();

            return _mapper.Map<IEnumerable<BlogCardDto>>(blogs).ToList();
        }

        [HttpGet("{id}", Name = "GetBlog")]
        [ProducesResponseType(typeof(BlogDetailDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<BlogDetailDto>> GetBlogDetail(int id)
        {
            _logger.LogInformation("Get detail blog with id " + id);
            var blog = await _repository.GetDetailAsync(id);

            if (blog == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(_mapper.Map<BlogDetailDto>(blog));
            }
        }
        [HttpGet("Related/{id}", Name = "GetRelatedBlog")]
        [ProducesResponseType(typeof(IEnumerable<BlogCardDto>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<BlogCardDto>>> GetRelatedBlogs(int id)
        {
            _logger.LogInformation("Get related blogs with id " + id);

            var blogs = await _repository.GetRelatedBlogsAsync(id);

            return _mapper.Map<IEnumerable<BlogCardDto>>(blogs).ToList();
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> CreateBlog([FromBody] CreateBlogDto blogDto)
        {
            _logger.LogInformation("Create blog");
            // save content in file
            var basePath = _configuration.GetValue<string>("StoragePaths:BlogContent");
            var current = DateTime.Now;
            // storage in path following: basePath/year/month/day/timestamp.txt
            var path = string.Join(basePath, Path.DirectorySeparatorChar, current.Year, Path.DirectorySeparatorChar, current.Month, Path.DirectorySeparatorChar, current.Day);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            // save to file
            try
            {
                await System.IO.File.WriteAllTextAsync(path + Path.DirectorySeparatorChar + $"{current.Ticks}.txt", blogDto.Content);
            }
            catch (Exception e)
            {
                _logger.LogError("Cannot save content to file: ", e);
                _logger.LogInformation("Content: " + blogDto.Content);
                _logger.LogInformation("Path: " + path + Path.DirectorySeparatorChar + $"{current.Ticks}.txt");
                return BadRequest();
            }

            var blogToCreate = _mapper.Map<AdminBlog>(blogDto);
            blogToCreate.ContentFilePath = string.Join(Path.DirectorySeparatorChar, current.Year, Path.DirectorySeparatorChar, current.Month, Path.DirectorySeparatorChar, current.Day, Path.DirectorySeparatorChar, $"{current.Ticks}.txt");

            var createdBlog = await _repository.CreateAsync(blogToCreate);

            return CreatedAtRoute("GetBlog", new { id = createdBlog.Id }, createdBlog);
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> UpdateBlog([FromBody] UpdateBlogDto blogDto)
        {
            _logger.LogInformation("Update blog");
            // save content in file
            var basePath = _configuration.GetValue<string>("StoragePaths:BlogContent");

            var currentBlog = await _repository.GetDetailAsync(blogDto.Id);

            if (currentBlog == null)
            {
                return NotFound();
            }
            else
            {
                // replace file content
                var path = string.Join(basePath, Path.DirectorySeparatorChar, currentBlog.ContentFilePath);

                // save to file
                try
                {
                    await System.IO.File.WriteAllTextAsync(path, blogDto.Content);
                }
                catch (IOException e)
                {
                    _logger.LogError("Cannot save content to file: ", e);
                    _logger.LogInformation("Content: " + blogDto.Content);
                    _logger.LogInformation("Path: " + path);
                    return BadRequest();
                }

            }

            var blogToUpdate = _mapper.Map<AdminBlog>(blogDto);

            await _repository.UpdateAsync(blogToUpdate);

            return Ok();
        }

        [HttpPost("ChangeStatus/{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> ChangeStatus(int id)
        {
            _logger.LogInformation("Change status of blog with id: " + id);
            var currentBlog = await _repository.GetDetailAsync(id);
            if (currentBlog == null)
            {
                return NotFound();
            }
            else if (!currentBlog.Status.HasValue)
            {
                return BadRequest();
            }
            else
            {
                await _repository.ChangeStatusAsync(currentBlog);
                return Ok();
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteBlog(int id)
        {
            _logger.LogInformation("Delete blog with id: " + id);
            var currentBlog = await _repository.GetDetailAsync(id);
            if (currentBlog == null)
            {
                return NotFound();
            }
            else
            {
                await _repository.DeleteAsync(currentBlog);
                return Ok();
            }
        }

    }
}
