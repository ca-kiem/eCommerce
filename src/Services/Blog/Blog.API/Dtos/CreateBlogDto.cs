﻿using System.ComponentModel.DataAnnotations;

namespace Blog.API.Dtos
{
    public class CreateBlogDto
    {
        [Required]
        [MaxLength(255)]
        public string Title { get; set; }
        [Required]
        public string CardImagePath { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        [MaxLength(255)]
        public string CreatedBy { get; set; }
        [Required]
        [MaxLength(255)]
        public string ShortDescription { get; set; }
    }
}
