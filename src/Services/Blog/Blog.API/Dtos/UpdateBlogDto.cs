﻿using System.ComponentModel.DataAnnotations;

namespace Blog.API.Dtos
{
    public class UpdateBlogDto
    {
        [Required]
        public int Id { get; set; }
        [MaxLength(255)]
        [Required]
        public string Title { get; set; }
        [MaxLength(255)]
        [Required]
        public string CardImagePath { get; set; }
        [Required]
        public string Content { get; set; }
        [MaxLength(255)]
        [Required]
        public string UpdatedBy { get; set; }
        [MaxLength(255)]
        [Required]
        public string ShortDescription { get; set; }
    }
}
