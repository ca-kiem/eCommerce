﻿using System;

namespace Blog.API.Dtos
{
    public class BlogListDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool? Status { get; set; }
    }
}
