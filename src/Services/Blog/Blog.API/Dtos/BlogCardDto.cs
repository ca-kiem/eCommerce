﻿using System;

namespace Blog.API.Dtos
{
    public class BlogCardDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string CardImagePath { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ShortDescription { get; set; }
    }
}
