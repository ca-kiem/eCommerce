﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blog.API.Models
{
    [Table("Blog")]
    public class AdminBlog : EntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Title of blog
        /// </summary>
        [MaxLength(255)]
        [Required]
        public string Title { get; set; }
        /// <summary>
        /// Short description in card view
        /// </summary>
        [MaxLength(255)]
        public string ShortDescription { get; set; }

        /// <summary>
        /// Image path file in card view
        /// </summary>
        [MaxLength(255)]
        [Required]
        public string CardImagePath { get; set; }

        /// <summary>
        /// Storage blog in a file
        /// </summary>
        [MaxLength(255)]
        [Required]
        public string ContentFilePath { get; set; }

        /// <summary>
        /// Total read by user
        /// </summary>
        public int TotalRead { get; set; }

        /// <summary>
        /// Status of blog
        /// true: Active
        /// false: Deactive
        /// null: Draft
        /// </summary>
        public bool? Status { get; set; }

    }
}
