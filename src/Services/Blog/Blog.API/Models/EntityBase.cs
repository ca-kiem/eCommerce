﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Blog.API.Models
{
    public class EntityBase
    {
        public DateTime CreatedDate { get; set; }
        [MaxLength(255)]
        public string CreatedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        [MaxLength(255)]
        public string LastModifiedBy { get; set; }
    }
}
