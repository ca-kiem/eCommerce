﻿using Blog.API.Helpers;
using Blog.API.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blog.API.Repository.Interfaces
{
    public interface IBlogRepository
    {
        Task<PageList<AdminBlog>> SearchBlogsAsync(BlogSearchParams param);
        Task<IEnumerable<AdminBlog>> GetRelatedBlogsAsync(int blogId);
        Task<AdminBlog> GetDetailAsync(int blogId);
        Task<AdminBlog> CreateAsync(AdminBlog blog);
        Task<AdminBlog> UpdateAsync(AdminBlog blog);
        Task DeleteAsync(AdminBlog blog);
        Task ChangeStatusAsync(AdminBlog blog);
        Task<IEnumerable<AdminBlog>> GetLastestAsync();
    }
}
