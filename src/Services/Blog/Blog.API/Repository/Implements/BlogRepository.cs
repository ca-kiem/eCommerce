﻿using Blog.API.Data;
using Blog.API.Helpers;
using Blog.API.Models;
using Blog.API.Repository.Interfaces;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.API.Repository.Implements
{
    public class BlogRepository : IBlogRepository
    {
        private readonly BlogContext _context;

        public BlogRepository(BlogContext context)
        {
            _context = context;
        }

        public async Task ChangeStatusAsync(AdminBlog blog)
        {
            blog.Status = !blog.Status.Value;
            _context.Entry(blog).State = EntityState.Modified;
            _context.Blogs.Update(blog);
            await _context.SaveChangesAsync();
        }

        public async Task<AdminBlog> CreateAsync(AdminBlog blog)
        {
            await _context.Blogs.AddAsync(blog);
            await _context.SaveChangesAsync();
            return blog;
        }

        public async Task DeleteAsync(AdminBlog blog)
        {
            _context.Blogs.Remove(blog);
            await _context.SaveChangesAsync();
        }

        public async Task<AdminBlog> GetDetailAsync(int blogId)
        {
            return await _context.Blogs.FindAsync(blogId);
        }

        public async Task<IEnumerable<AdminBlog>> GetLastestAsync()
        {
            return await _context.Blogs
                .Take(3).Skip(0)
                .OrderByDescending(x => x.CreatedDate)
                .ToListAsync();
        }

        /// <summary>
        /// Take first 5 blogs not contains current blog
        /// </summary>
        /// <param name="blogId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<AdminBlog>> GetRelatedBlogsAsync(int blogId)
        {
            return await _context.Blogs
                .Where(x => x.Id != blogId)
                .Take(3).Skip(0)
                .OrderByDescending(x => x.CreatedDate)
                .ToListAsync();
        }

        public async Task<PageList<AdminBlog>> SearchBlogsAsync(BlogSearchParams param)
        {
            var query = _context.Blogs.AsQueryable();
            if (!string.IsNullOrEmpty(param.Content))
            {
                query = query.Where(x => x.Title.Contains(param.Content, StringComparison.OrdinalIgnoreCase));
            }

            if (!string.IsNullOrEmpty(param.CreatedBy))
            {
                query = query.Where(x => x.CreatedBy.Equals(param.CreatedBy));
            }

            query.OrderByDescending(x => x.CreatedDate);

            return await PageList<AdminBlog>.CreateAsync(query, param);
        }

        public async Task<AdminBlog> UpdateAsync(AdminBlog blog)
        {
            _context.Entry(blog).State = EntityState.Modified;

            // exclude property not update
            _context.Entry(blog).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(blog).Property(x => x.CreatedDate).IsModified = false;
            _context.Entry(blog).Property(x => x.ContentFilePath).IsModified = false;

            _context.Update(blog);


            await _context.SaveChangesAsync();
            return blog;
        }
    }
}
