﻿using Blog.API.Repository.Implements;
using Blog.API.Repository.Interfaces;

using Microsoft.Extensions.DependencyInjection;


namespace System.API.Helpers
{
    public static class DIHelper
    {
        internal static IServiceCollection InitDI(this IServiceCollection services)
        {
            services.AddScoped<IBlogRepository, BlogRepository>();

            return services;
        }
    }
}
