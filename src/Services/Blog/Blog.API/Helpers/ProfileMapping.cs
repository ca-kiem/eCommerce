﻿using AutoMapper;

using Blog.API.Dtos;
using Blog.API.Models;

using Microsoft.Extensions.Configuration;

using System.IO;

namespace Blog.API.Helpers
{
    public class ProfileMapping : Profile
    {
        public ProfileMapping(IConfiguration configuration)
        {
            CreateMap<CreateBlogDto, AdminBlog>();
            CreateMap<UpdateBlogDto, AdminBlog>();
            CreateMap<AdminBlog, BlogCardDto>()
                .ForMember(x => x.CardImagePath, x => x.MapFrom(y => string.Join(configuration.GetValue<string>("StoragePaths:Image"), Path.DirectorySeparatorChar, y.CardImagePath)));
            CreateMap<AdminBlog, BlogListDto>();
            CreateMap<AdminBlog, BlogDetailDto>();
        }
    }
}
