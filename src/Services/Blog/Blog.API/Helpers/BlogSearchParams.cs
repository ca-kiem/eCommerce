﻿using System.Collections.Generic;

namespace Blog.API.Helpers
{
    public class BlogSearchParams : BaseParams
    {
        public BlogSearchParams()
        {
            Status = new List<bool?>();
        }
        public string Content { get; set; }
        public string CreatedBy { get; set; }
        public IEnumerable<bool?> Status { get; set; }
    }
}
