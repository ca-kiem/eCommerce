﻿using Basket.API.Entities;

using System.Threading.Tasks;

namespace Basket.API.Repository.Interface
{
    public interface IBasketRepository
    {
        Task<ShoppingCart> GetBasketAsync(string userId);
        Task<ShoppingCart> UpdateBasketAsync(ShoppingCart basket);
        Task DeleteAsync(string userId);
    }
}
