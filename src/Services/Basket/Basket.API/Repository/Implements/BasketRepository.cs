﻿using Basket.API.Entities;
using Basket.API.Repository.Interface;

using Microsoft.Extensions.Caching.Distributed;

using System.Text.Json;
using System.Threading.Tasks;

namespace Basket.API.Repository.Implements
{
    public class BasketRepository : IBasketRepository
    {
        private readonly IDistributedCache _cache;

        public BasketRepository(IDistributedCache cache)
        {
            _cache = cache;
        }

        public async Task DeleteAsync(string userId)
        {
            await _cache.RemoveAsync(userId.ToString());
        }

        public async Task<ShoppingCart> GetBasketAsync(string userId)
        {
            var basketStr = await _cache.GetAsync(userId.ToString());
            if (basketStr == null)
            {
                return null;
            }
            else
            {
                return JsonSerializer.Deserialize<ShoppingCart>(basketStr);
            }
        }

        public async Task<ShoppingCart> UpdateBasketAsync(ShoppingCart basket)
        {
            await _cache.SetStringAsync(basket.UserId.ToString(), JsonSerializer.Serialize(basket));
            return basket;
        }
    }
}
