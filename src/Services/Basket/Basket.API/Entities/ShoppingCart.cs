﻿using EventBus.Messages.Events;

using System.Collections.Generic;
using System.Linq;

namespace Basket.API.Entities
{
    public class ShoppingCart
    {
        public ShoppingCart()
        {
            Items = new List<ShoppingCartItem>();
        }
        public string UserId { get; set; }
        public decimal TotalPrice { 
            get 
            {
                return Items.Sum(x => x.Quantity * x.Price);
            } 
        }

        public IList<ShoppingCartItem> Items { get; set; }
    }
}
