﻿using Basket.API.Repository.Implements;
using Basket.API.Repository.Interface;

using Microsoft.Extensions.DependencyInjection;

namespace Basket.API.Helpers
{
    public class DIHelper
    {
        internal static void Init(IServiceCollection services)
        {
            services.AddScoped<IBasketRepository, BasketRepository>();
        }
    }
}
