﻿using AutoMapper;

using Basket.API.Entities;

using EventBus.Messages.Events;

namespace Basket.API.Helpers
{
    public class ProfileMapping : Profile
    {
        public ProfileMapping()
        {
            CreateMap<BasketCheckout, BasketCheckoutEvent>();
        }
    }
}
