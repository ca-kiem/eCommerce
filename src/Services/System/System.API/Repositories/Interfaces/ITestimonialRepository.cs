﻿using System.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace System.API.Repositories.Interfaces
{
    public interface ITestimonialRepository : IBaseRepository<Testimonial>
    {
        Task<IEnumerable<Testimonial>> GetAllAsync();
        Task<Testimonial> GetDetailAsync(string id);
    }
}
