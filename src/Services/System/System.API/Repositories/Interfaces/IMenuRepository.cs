﻿using System.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace System.API.Repositories.Interfaces
{
    public interface IMenuRepository : IBaseRepository<Menu>
    {
        Task<IEnumerable<Menu>> GetAsync();
        Task<Menu> GetDetailAsync(string id);
    }
}
