﻿using System.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace System.API.Repositories.Interfaces
{
    public interface ICategoryRepository : IBaseRepository<Category>
    {
        Task<IEnumerable<Category>> GetAsync();
        Task<IEnumerable<Category>> GetChildrenAsync(string parentId);
        Task<Category> GetDetailAsync(string id);
    }
}
