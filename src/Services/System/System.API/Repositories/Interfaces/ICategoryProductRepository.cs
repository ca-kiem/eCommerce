﻿using System.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace System.API.Repositories.Interfaces
{
    public interface ICategoryProductRepository : IBaseRepository<CategoryProduct>
    {
        Task<IEnumerable<CategoryProduct>> GetAsync();
        Task<CategoryProduct> GetDetailAsync(string id);
    }
}
