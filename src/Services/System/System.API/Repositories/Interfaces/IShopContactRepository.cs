﻿using System.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace System.API.Repositories.Interfaces
{
    public interface IShopContactRepository : IBaseRepository<ShopContact>
    {
        Task<IEnumerable<ShopContact>> GetAsync();
        Task<ShopContact> GetDetailAsync(string id);
    }
}
