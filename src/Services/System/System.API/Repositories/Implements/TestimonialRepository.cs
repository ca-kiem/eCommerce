﻿using MongoDB.Driver;

using System.API.Data;
using System.API.Models;
using System.API.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace System.API.Repositories.Implements
{
    public class TestimonialRepository : ITestimonialRepository
    {
        private readonly ISystemContext _context;

        public TestimonialRepository(ISystemContext context)
        {
            _context = context;
        }

        public async Task CreateAsync(Testimonial entity)
        {
            await _context.Testimonials.InsertOneAsync(entity);
        }

        public async Task<bool> DeleteAsync(string id)
        {
            var deleteResult = await _context.Testimonials.DeleteOneAsync(x => x.Id == id);
            return deleteResult.IsAcknowledged && deleteResult.DeletedCount == 1;
        }

        public async Task<IEnumerable<Testimonial>> GetAllAsync()
        {
            return await _context.Testimonials.Find(x => true).ToListAsync();
        }

        public async Task<bool> UpdateAsync(Testimonial entity)
        {
            var updateResult = await _context.Testimonials.ReplaceOneAsync(x => x.Id == entity.Id, entity);
            return updateResult.IsAcknowledged && updateResult.ModifiedCount == 1;
        }

        public async Task<Testimonial> GetDetailAsync(string id)
        {
            return await _context.Testimonials.Find(x => x.Id == id).FirstOrDefaultAsync();
        }
    }
}
