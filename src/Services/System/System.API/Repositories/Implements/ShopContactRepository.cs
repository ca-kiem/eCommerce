﻿using MongoDB.Driver;

using System.API.Data;
using System.API.Models;
using System.API.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace System.API.Repositories.Implements
{
    public class ShopContactRepository : IShopContactRepository
    {
        private readonly ISystemContext _context;

        public ShopContactRepository(ISystemContext context)
        {
            _context = context;
        }

        public async Task CreateAsync(ShopContact entity)
        {
            await _context.ShopContacts.InsertOneAsync(entity);
        }

        public async Task<bool> DeleteAsync(string id)
        {
            var deleteResult = await _context.ShopContacts.DeleteOneAsync(x => x.Id == id);
            return deleteResult.IsAcknowledged && deleteResult.DeletedCount == 1;
        }

        public async Task<IEnumerable<ShopContact>> GetAsync()
        {
            return await _context.ShopContacts.Find(x => true).ToListAsync();
        }

        public async Task<ShopContact> GetDetailAsync(string id)
        {
            return await _context.ShopContacts.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<bool> UpdateAsync(ShopContact entity)
        {
            var updateResult = await _context.ShopContacts.ReplaceOneAsync(x => x.Id == entity.Id, entity);
            return updateResult.IsAcknowledged && updateResult.ModifiedCount == 1;
        }
    }
}
