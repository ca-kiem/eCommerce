﻿using MongoDB.Driver;

using System;
using System.API.Data;
using System.API.Models;
using System.API.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.API.Repositories.Implements
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly ISystemContext _context;

        public CategoryRepository(ISystemContext context)
        {
            _context = context;
        }

        public async Task CreateAsync(Category entity)
        {
            await _context.Categories.InsertOneAsync(entity);
        }

        public async Task<bool> DeleteAsync(string id)
        {
            var deleteResult = await _context.Categories.DeleteOneAsync(x => x.Id == id);
            return deleteResult.IsAcknowledged && deleteResult.DeletedCount == 1;
        }

        public async Task<IEnumerable<Category>> GetAsync()
        {
            return await _context.Categories.Find(x => true).ToListAsync();
        }

        public async Task<IEnumerable<Category>> GetChildrenAsync(string parentId)
        {
            return await _context.Categories.Find(x => x.ParentId == parentId).ToListAsync();
        }

        public async Task<Category> GetDetailAsync(string id)
        {
            return await _context.Categories.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<bool> UpdateAsync(Category entity)
        {
            var updateResult = await _context.Categories.ReplaceOneAsync(x => x.Id == entity.Id, entity);
            return updateResult.IsAcknowledged && updateResult.ModifiedCount == 1;
        }
    }
}
