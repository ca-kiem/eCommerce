﻿using MongoDB.Driver;

using System.API.Data;
using System.API.Models;
using System.API.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace System.API.Repositories.Implements
{
    public class CategoryProductRepository : ICategoryProductRepository
    {
        private readonly ISystemContext _context;

        public CategoryProductRepository(ISystemContext context)
        {
            _context = context;
        }

        public async Task CreateAsync(CategoryProduct entity)
        {
            await _context.CategoryProducts.InsertOneAsync(entity);
        }

        public async Task<bool> DeleteAsync(string id)
        {
            var deleteResult = await _context.CategoryProducts.DeleteOneAsync(x => x.Id == id);
            return deleteResult.IsAcknowledged && deleteResult.DeletedCount == 1;
        }

        public async Task<IEnumerable<CategoryProduct>> GetAsync()
        {
            return await _context.CategoryProducts.Find(x => true).ToListAsync();
        }

        public async Task<CategoryProduct> GetDetailAsync(string id)
        {
            return await _context.CategoryProducts.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<bool> UpdateAsync(CategoryProduct entity)
        {
            var updateResult = await _context.CategoryProducts.ReplaceOneAsync(x => x.Id == entity.Id, entity);
            return updateResult.IsAcknowledged && updateResult.ModifiedCount == 1;
        }
    }
}
