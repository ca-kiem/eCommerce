﻿using MongoDB.Driver;

using System.API.Data;
using System.API.Models;
using System.API.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace System.API.Repositories.Implements
{
    public class MenuRepository : IMenuRepository
    {
        private readonly ISystemContext _context;

        public MenuRepository(ISystemContext context)
        {
            _context = context;
        }

        public async Task CreateAsync(Menu entity)
        {
            await _context.Menus.InsertOneAsync(entity);
        }

        public async Task<bool> DeleteAsync(string id)
        {
            var deleteResult = await _context.Menus.DeleteOneAsync(x => x.Id == id);
            return deleteResult.IsAcknowledged && deleteResult.DeletedCount == 1;
        }

        public async Task<IEnumerable<Menu>> GetAsync()
        {
            return await _context.Menus.Find(x => true).ToListAsync();
        }

        public async Task<Menu> GetDetailAsync(string id)
        {
            return await _context.Menus.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<bool> UpdateAsync(Menu entity)
        {
            var updateResult = await _context.Menus.ReplaceOneAsync(x => x.Id == entity.Id, entity);
            return updateResult.IsAcknowledged && updateResult.ModifiedCount == 1;
        }
    }
}
