﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using MongoDB.Bson;

using System;
using System.API.Models;
using System.API.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace System.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CategoryProductController : ControllerBase
    {
        private readonly ICategoryProductRepository _repository;
        private readonly ILogger<CategoryProductController> _logger;

        public CategoryProductController(ICategoryProductRepository repository, ILogger<CategoryProductController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<CategoryProduct>), ((int)HttpStatusCode.OK))]
        public async Task<ActionResult<IEnumerable<CategoryProduct>>> GetCategorys()
        {
            _logger.LogInformation("Get categories");
            return Ok(await _repository.GetAsync());
        }

        [HttpGet("{id:length(24)}", Name = "GetCategoryProduct")]
        [ProducesResponseType(typeof(CategoryProduct), ((int)HttpStatusCode.OK))]
        [ProducesResponseType(((int)HttpStatusCode.NotFound))]
        public async Task<ActionResult<CategoryProduct>> GetCategoryById(string id)
        {
            var categoryProduct = await _repository.GetDetailAsync(id);

            if (categoryProduct == null)
            {
                _logger.LogInformation($"Cannot found categoryProduct with id: {id}");
                return NotFound();
            }
            else
            {
                return Ok(categoryProduct);
            }
        }

        [HttpPost]
        [ProducesResponseType(typeof(CategoryProduct), ((int)HttpStatusCode.OK))]
        public async Task<IActionResult> PostCategory(CategoryProduct categoryProduct)
        {
            categoryProduct.Id = ObjectId.GenerateNewId().ToString();
            await _repository.CreateAsync(categoryProduct);

            return CreatedAtRoute("GetCategoryProduct", new { id = categoryProduct.Id }, categoryProduct);
        }

        [HttpDelete("{id:length(24)}")]
        [ProducesResponseType(((int)HttpStatusCode.OK))]
        [ProducesResponseType(((int)HttpStatusCode.BadRequest))]
        public async Task<IActionResult> DeleteCategory(string id)
        {
            var result = await _repository.DeleteAsync(id);
            if (result)
            {

                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut]
        [ProducesResponseType(typeof(CategoryProduct), ((int)HttpStatusCode.OK))]
        [ProducesResponseType(((int)HttpStatusCode.NotFound))]
        [ProducesResponseType(((int)HttpStatusCode.BadRequest))]
        public async Task<IActionResult> PutCategory(CategoryProduct categoryProduct)
        {
            // check categoryProduct exist
            if (await _repository.GetDetailAsync(categoryProduct.Id) == null)
            {
                _logger.LogInformation($"Cannot found categoryProduct with id: {categoryProduct.Id}");
                return NotFound();
            }
            else
            {
                var result = await _repository.UpdateAsync(categoryProduct);

                if (result)
                {
                    return Ok();
                }
                else
                {
                    _logger.LogInformation($"Cannot update categoryProduct with id: {categoryProduct.Id}");
                    return BadRequest();
                }
            }
        }
    }
}
