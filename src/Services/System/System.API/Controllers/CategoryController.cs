﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using MongoDB.Bson;

using System.API.Models;
using System.API.Repositories.Interfaces;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Catalog.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryRepository _repository;
        private readonly ILogger<CategoryController> _logger;

        public CategoryController(ICategoryRepository repository, ILogger<CategoryController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Category>), ((int)HttpStatusCode.OK))]
        public async Task<ActionResult<IEnumerable<Category>>> GetCategorys()
        {
            _logger.LogInformation("Get categories");
            return Ok(await _repository.GetAsync());
        }

        [HttpGet("{id:length(24)}", Name = "GetCategory")]
        [ProducesResponseType(typeof(Category), ((int)HttpStatusCode.OK))]
        [ProducesResponseType(((int)HttpStatusCode.NotFound))]
        public async Task<ActionResult<Category>> GetCategoryById(string id)
        {
            var category = await _repository.GetDetailAsync(id);

            if (category == null)
            {
                _logger.LogInformation($"Cannot found category with id: {id}");
                return NotFound();
            }
            else
            {
                return Ok(category);
            }
        }

        [HttpGet("GetChildren/{id:length(24)}", Name = "GetChildren")]
        [ProducesResponseType(typeof(IEnumerable<Category>), ((int)HttpStatusCode.OK))]
        [ProducesResponseType(((int)HttpStatusCode.NotFound))]
        public async Task<ActionResult<IEnumerable<Category>>> GetChildren(string parentId)
        {
            var categories = await _repository.GetChildrenAsync(parentId);

            return Ok(categories);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Category), ((int)HttpStatusCode.OK))]
        public async Task<IActionResult> PostCategory(Category category)
        {
            category.Id = ObjectId.GenerateNewId().ToString();
            await _repository.CreateAsync(category);

            return CreatedAtRoute("GetCategory", new { id = category.Id }, category);
        }

        [HttpDelete("{id:length(24)}")]
        [ProducesResponseType(((int)HttpStatusCode.OK))]
        [ProducesResponseType(((int)HttpStatusCode.BadRequest))]
        public async Task<IActionResult> DeleteCategory(string id)
        {
            var result = await _repository.DeleteAsync(id);
            if (result)
            {

                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut]
        [ProducesResponseType(typeof(Category), ((int)HttpStatusCode.OK))]
        [ProducesResponseType(((int)HttpStatusCode.NotFound))]
        [ProducesResponseType(((int)HttpStatusCode.BadRequest))]
        public async Task<IActionResult> PutCategory(Category category)
        {
            // check category exist
            if (await _repository.GetDetailAsync(category.Id) == null)
            {
                _logger.LogInformation($"Cannot found category with id: {category.Id}");
                return NotFound();
            }
            else
            {
                var result = await _repository.UpdateAsync(category);

                if (result)
                {
                    return Ok();
                }
                else
                {
                    _logger.LogInformation($"Cannot update category with id: {category.Id}");
                    return BadRequest();
                }
            }
        }
    }
}
