﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using MongoDB.Bson;

using System.API.Models;
using System.API.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Catalog.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ShopContactController : ControllerBase
    {
        private readonly IShopContactRepository _repository;
        private readonly ILogger<ShopContactController> _logger;

        public ShopContactController(IShopContactRepository repository, ILogger<ShopContactController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ShopContact>), ((int)HttpStatusCode.OK))]
        public async Task<ActionResult<IEnumerable<ShopContact>>> GetShopContacts()
        {
            _logger.LogInformation("Get shop contacts");
            return Ok((await _repository.GetAsync()).FirstOrDefault());
        }

        [HttpGet("{id:length(24)}", Name = "GetShopContact")]
        [ProducesResponseType(typeof(ShopContact), ((int)HttpStatusCode.OK))]
        [ProducesResponseType(((int)HttpStatusCode.NotFound))]
        public async Task<ActionResult<ShopContact>> GetShopContactById(string id)
        {
            var shopContact = await _repository.GetDetailAsync(id);

            if (shopContact == null)
            {
                _logger.LogInformation($"Cannot found shopContact with id: {id}");
                return NotFound();
            }
            else
            {
                return Ok(shopContact);
            }
        }

        [HttpPost]
        [ProducesResponseType(typeof(ShopContact), ((int)HttpStatusCode.OK))]
        public async Task<IActionResult> PostShopContact(ShopContact shopContact)
        {
            shopContact.Id = ObjectId.GenerateNewId().ToString();
            await _repository.CreateAsync(shopContact);

            return CreatedAtRoute("GetShopContact", new { id = shopContact.Id }, shopContact);
        }

        [HttpPut]
        [ProducesResponseType(typeof(ShopContact), ((int)HttpStatusCode.OK))]
        [ProducesResponseType(((int)HttpStatusCode.NotFound))]
        [ProducesResponseType(((int)HttpStatusCode.BadRequest))]
        public async Task<IActionResult> PutShopContact(ShopContact shopContact)
        {
            // check shop contact exist
            if (await _repository.GetDetailAsync(shopContact.Id) == null)
            {
                _logger.LogInformation($"Cannot found shop contact with id: {shopContact.Id}");
                return NotFound();
            }
            else
            {
                var result = await _repository.UpdateAsync(shopContact);

                if (result)
                {
                    return Ok();
                }
                else
                {
                    _logger.LogInformation($"Cannot update shop contact with id: {shopContact.Id}");
                    return BadRequest();
                }
            }
        }
    }
}
