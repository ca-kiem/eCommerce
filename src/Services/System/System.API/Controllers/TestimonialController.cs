﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using MongoDB.Bson;

using System.API.Models;
using System.API.Repositories.Interfaces;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Catalog.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class TestimonialController : ControllerBase
    {
        private readonly ITestimonialRepository _repository;
        private readonly ILogger<TestimonialController> _logger;

        public TestimonialController(ITestimonialRepository repository, ILogger<TestimonialController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Testimonial>), ((int)HttpStatusCode.OK))]
        public async Task<ActionResult<IEnumerable<Testimonial>>> GetTestimonials()
        {
            _logger.LogInformation("Get testimonials");
            return Ok(await _repository.GetAllAsync());
        }

        [HttpGet("{id:length(24)}", Name = "GetTestimonial")]
        [ProducesResponseType(typeof(Testimonial), ((int)HttpStatusCode.OK))]
        [ProducesResponseType(((int)HttpStatusCode.NotFound))]
        public async Task<ActionResult<Testimonial>> GetTestimonialById(string id)
        {
            var testimonial = await _repository.GetDetailAsync(id);

            if (testimonial == null)
            {
                _logger.LogInformation($"Cannot found Testimonial with id: {id}");
                return NotFound();
            }
            else
            {
                return Ok(testimonial);
            }
        }

        [HttpPost]
        [ProducesResponseType(typeof(Testimonial), ((int)HttpStatusCode.OK))]
        public async Task<IActionResult> PostTestimonial(Testimonial testimonial)
        {
            testimonial.Id = ObjectId.GenerateNewId().ToString();
            await _repository.CreateAsync(testimonial);

            return CreatedAtRoute("GetTestimonial", new { id = testimonial.Id }, testimonial);
        }

        [HttpPut]
        [ProducesResponseType(typeof(Testimonial), ((int)HttpStatusCode.OK))]
        [ProducesResponseType(((int)HttpStatusCode.NotFound))]
        [ProducesResponseType(((int)HttpStatusCode.BadRequest))]
        public async Task<IActionResult> PutTestimonial(Testimonial testimonial)
        {
            // check testimonial exist
            if (await _repository.GetDetailAsync(testimonial.Id) == null)
            {
                _logger.LogInformation($"Cannot found testimonial with id: {testimonial.Id}");
                return NotFound();
            }
            else
            {
                var result = await _repository.UpdateAsync(testimonial);

                if (result)
                {
                    return Ok();
                }
                else
                {
                    _logger.LogInformation($"Cannot update testimonial with id: {testimonial.Id}");
                    return BadRequest();
                }
            }
        }
    }
}
