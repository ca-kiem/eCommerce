﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using MongoDB.Bson;

using System.API.Models;
using System.API.Repositories.Interfaces;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Catalog.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class MenuController : ControllerBase
    {
        private readonly IMenuRepository _repository;
        private readonly ILogger<MenuController> _logger;

        public MenuController(IMenuRepository repository, ILogger<MenuController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Menu>), ((int)HttpStatusCode.OK))]
        public async Task<ActionResult<IEnumerable<Menu>>> GetMenus()
        {
            _logger.LogInformation("Get menus");
            return Ok(await _repository.GetAsync());
        }

        [HttpGet("{id:length(24)}", Name = "GetMenu")]
        [ProducesResponseType(typeof(Menu), ((int)HttpStatusCode.OK))]
        [ProducesResponseType(((int)HttpStatusCode.NotFound))]
        public async Task<ActionResult<Menu>> GetMenuById(string id)
        {
            var menu = await _repository.GetDetailAsync(id);

            if (menu == null)
            {
                _logger.LogInformation($"Cannot found menu with id: {id}");
                return NotFound();
            }
            else
            {
                return Ok(menu);
            }
        }

        [HttpPost]
        [ProducesResponseType(typeof(Menu), ((int)HttpStatusCode.OK))]
        public async Task<IActionResult> PostMenu(Menu menu)
        {
            menu.Id = ObjectId.GenerateNewId().ToString();
            await _repository.CreateAsync(menu);

            return CreatedAtRoute("GetMenu", new { id = menu.Id }, menu);
        }

        [HttpDelete("{id:length(24)}")]
        [ProducesResponseType(((int)HttpStatusCode.OK))]
        [ProducesResponseType(((int)HttpStatusCode.BadRequest))]
        public async Task<IActionResult> DeleteMenu(string id)
        {
            var result = await _repository.DeleteAsync(id);
            if (result)
            {

                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut]
        [ProducesResponseType(typeof(Menu), ((int)HttpStatusCode.OK))]
        [ProducesResponseType(((int)HttpStatusCode.NotFound))]
        [ProducesResponseType(((int)HttpStatusCode.BadRequest))]
        public async Task<IActionResult> PutMenu(Menu menu)
        {
            // check menu exist
            if (await _repository.GetDetailAsync(menu.Id) == null)
            {
                _logger.LogInformation($"Cannot found menu with id: {menu.Id}");
                return NotFound();
            }
            else
            {
                var result = await _repository.UpdateAsync(menu);

                if (result)
                {
                    return Ok();
                }
                else
                {
                    _logger.LogInformation($"Cannot update menu with id: {menu.Id}");
                    return BadRequest();
                }
            }
        }
    }
}
