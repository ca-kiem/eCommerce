﻿using Microsoft.Extensions.DependencyInjection;

using System.API.Data;
using System.API.Repositories.Implements;
using System.API.Repositories.Interfaces;

namespace System.API.Helpers
{
    public static class DIHelper
    {
        internal static IServiceCollection InitDI(this IServiceCollection services)
        {
            services.AddScoped<ISystemContext, SystemContext>();

            services.AddScoped<IShopContactRepository, ShopContactRepository>();
            services.AddScoped<IMenuRepository, MenuRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<ICategoryProductRepository, CategoryProductRepository>();
            services.AddScoped<ITestimonialRepository, TestimonialRepository>();

            return services;
        }
    }
}
