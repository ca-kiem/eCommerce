﻿using MongoDB.Driver;

using System.API.Models;

namespace System.API.Data
{
    public interface ISystemContext
    {
        IMongoCollection<Menu> Menus { get; }
        IMongoCollection<Testimonial> Testimonials { get; }
        IMongoCollection<ShopContact> ShopContacts { get; }
        IMongoCollection<Category> Categories { get; }
        IMongoCollection<CategoryProduct> CategoryProducts { get; }
    }
}
