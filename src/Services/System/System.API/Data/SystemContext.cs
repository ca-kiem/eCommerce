﻿using Microsoft.Extensions.Configuration;

using MongoDB.Driver;

using System.API.Models;

namespace System.API.Data
{
    public class SystemContext : ISystemContext
    {
        public IMongoCollection<Menu> Menus { get; }

        public IMongoCollection<ShopContact> ShopContacts { get; }
        public IMongoCollection<Testimonial> Testimonials { get; }
        public IMongoCollection<Category> Categories { get; }
        public IMongoCollection<CategoryProduct> CategoryProducts { get; }

        public SystemContext(IConfiguration configuration)
        {
            var client = new MongoClient(configuration.GetValue<string>("DatabaseSettings:ConnectionString"));
            var database = client.GetDatabase(configuration.GetValue<string>("DatabaseSettings:DatabaseName"));

            Menus = database.GetCollection<Menu>(configuration.GetValue<string>("DatabaseSettings:CollectionNames:Menu"));
            ShopContacts = database.GetCollection<ShopContact>(configuration.GetValue<string>("DatabaseSettings:CollectionNames:ShopContact"));
            Categories = database.GetCollection<Category>(configuration.GetValue<string>("DatabaseSettings:CollectionNames:Category"));
            CategoryProducts = database.GetCollection<CategoryProduct>(configuration.GetValue<string>("DatabaseSettings:CollectionNames:CategoryProduct"));
            Testimonials = database.GetCollection<Testimonial>(configuration.GetValue<string>("DatabaseSettings:CollectionNames:Testimonial"));
        }
    }
}
