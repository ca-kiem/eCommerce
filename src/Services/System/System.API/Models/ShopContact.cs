﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace System.API.Models
{
    public class ShopContact
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement]
        public string Name { get; set; }
        [BsonElement]
        public string Email { get; set; }
        [BsonElement]
        public string PhoneNumber { get; set; }
        [BsonElement]
        public string Address { get; set; }
        [BsonElement]
        public string FacebookUrl { get; set; }
        [BsonElement]
        public string InstagramUrl { get; set; }
    }
}
