﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace System.API.Models
{
    public class Testimonial
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement]
        public string Quote { get; set; }
        [BsonElement]
        public string Name { get; set; }
        [BsonElement]
        public string Type { get; set; }
    }
}
