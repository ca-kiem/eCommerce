﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace System.API.Models
{
    public class Category
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement]
        public string Name { get; set; }

        [BsonElement]
        public string Code { get; set; }

        [BsonElement]
        public string ParentId { get; set; }
    }
}
