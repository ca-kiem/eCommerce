﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

using System.Collections.Generic;

namespace System.API.Models
{
    /// <summary>
    /// Category with products in home page
    /// </summary>
    public class CategoryProduct
    {
        public CategoryProduct()
        {
            ProductIds = new List<string>();
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement]
        public string Name { get; set; }
        [BsonElement]
        public IEnumerable<string> ProductIds { get; set; }
    }
}
