﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace System.API.Models
{
    public class Menu
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement]
        public string Name { get; set; }

        [BsonElement]
        public string Url { get; set; }

        [BsonElement]
        public string ParentId { get; set; }
        
        [BsonElement]
        public int Order { get; set; }
    }
}
