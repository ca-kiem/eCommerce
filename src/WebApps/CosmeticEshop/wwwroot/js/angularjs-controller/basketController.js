﻿var app = angular.module('basket-app', []);

app.controller('BasketController', function ($scope, $http) {
    const basket = this;
    const init = function () {
        $http.get('/Basket/GetBasket').then(function (response) {
            $scope.items = response.data.items;
        });
    }

    init();

    basket.checkout = function () {
        $.ajax({
            url: '/Basket/UpdateBasketAndCheckout',
            data: {
                basket: response.items
            },
            type: 'POST',
        }).done(function (response) {
        });
    }

    basket.remove = function (productId) {

        const removeItemInBasket = function () {
            $scope.items = $scope.items.filter(function (item) {
                return item.productId !== productId;
            });

            $scope.$apply();
        }
        removeItem(productId, removeItemInBasket);
    };

    basket.removeBasket = function () {
        const removeBasket = function () {
            $scope.items = [];

            $scope.$apply();
        }

        emptyBasket(removeBasket);
    }

    basket.getTotalPrice = function () {
        var total = 0;

        for (var i = 0; $scope.items && i < $scope.items.length; i++) {
            var product = $scope.items[i];
            total += (product.price * product.quantity);
        }
        return total;
    }

    basket.getTotalSale = function () {
        var total = 0;
        for (var i = 0; $scope.items && i < $scope.items.length; i++) {
            var product = $scope.items[i];
            total += (product.sale * product.quantity);
        }
        return total;
    }

    basket.changeQuantity = function (productId, change) {
        const item = $scope.items.filter(function (item) {
            return item.productId === productId;
        })[0];

        if (!item || item.quantity === 1 && change === -1) {
            basket.remove(productId);
        } else {

            successCallback = function () {
                item.quantity += change;
            };

            setQuantity(productId, item.quantity, successCallback);
        }
    }

    basket.updateBasket = function () {
        init();

        $scope.$apply();
    }

});