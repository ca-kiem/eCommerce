﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/basketHub").build();

async function start() {
    try {
        await connection.start();
        console.log("SignalR Connected.");
    } catch (err) {
        console.log(err);
        setTimeout(start, 5000);
    }
};

connection.onclose(start);

// Start the connection.
start();

connection.on("SetBasket", function (totalItems) {
    setTotalItems(totalItems)

    $.ajax({
        url: '/Basket/SyncBasket',
        type: 'GET',
        contentType: 'html'
    }).done(function (result) {
        $('#mini-cart-view').html(result);
    })
});
