﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function setTotalItems(quantity) {
    $('[data-mfp-src="#miniCart-popup"] span.cart-count').html(quantity);
}

function addToCart(productId, quantity, hideNotify) {
    $.ajax({
        url: '/Basket/AddItemToBasket',
        data: {
            productId,
            quantity
        },
        type: 'POST',
    }).done(function () {
        // notify success
        if (!hideNotify) {
            success("Thêm sản phẩm thành công!");
        }
    })
        .fail(function (result) {
            error("Có lỗi xảy ra trong quá trình xử lý. Vui lòng thử lại sau hoặc liên hệ trực tiếp với chúng tôi để được hỗ trợ.")
        });
}
function setQuantity(productId, quantity, successCallback) {
    $.ajax({
        url: '/Basket/SetQuantityProductInBasket',
        data: {
            productId,
            quantity
        },
        type: 'POST',
    }).done(function () {
        if (successCallback) {
            successCallback();
        }
    })
        .fail(function (result) {
            error("Có lỗi xảy ra trong quá trình xử lý. Vui lòng thử lại sau hoặc liên hệ trực tiếp với chúng tôi để được hỗ trợ.")
        });
}

function removeItemCallback(productId, callbackSuccessFn) {
    $.ajax({
        url: '/Basket/RemoveItemFromBasket',
        data: {
            productId
        },
        type: 'POST'
    }).done(function () {
        // notify success
        success("Xóa sản phẩm thành công!");
        if (callbackSuccessFn) {
            callbackSuccessFn();
        }
    })
        .fail(function (result) {
            error("Có lỗi xảy ra trong quá trình xử lý. Vui lòng thử lại sau hoặc liên hệ trực tiếp với chúng tôi để được hỗ trợ.")
        });
}

function emptyBasketCallback(callbackSuccessFn) {
    $.ajax({
        url: '/Basket/EmptyBasket',
        type: 'POST'
    }).done(function () {
        // notify success
        success("Xóa giỏ hàng thành công!");
        if (callbackSuccessFn) {
            callbackSuccessFn();
        }
    })
        .fail(function (result) {
            error("Có lỗi xảy ra trong quá trình xử lý. Vui lòng thử lại sau hoặc liên hệ trực tiếp với chúng tôi để được hỗ trợ.")
        });
}

function removeItem(productId, callbackSuccessFn) {
    confirmRemoveItem(productId, callbackSuccessFn);
}

function emptyBasket(callbackSuccessFn) {
    Swal.fire({
        title: 'Xóa tất cả sản phẩm khỏi giỏ hàng',
        text: "Bạn có chắc chắn muốn xóa tất cả sản phẩm khỏi giỏ hàng không?",
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Không',
        confirmButtonText: 'Xóa tất cả!',
        customClass: {
            cancelButton: 'cancel-btn-remove-item',
            confirmButton: 'confirm-btn-remove-item'
        }
    }).then((result) => {
        if (result.isConfirmed) {
            emptyBasketCallback(callbackSuccessFn);
        }
    })
}
// START NOTIFY JS 
function success(content) {
    $.notify(content, {
        clickToHide: true,
        position: 'bottom right',
        className: 'success'
    })
}

function error(content) {
    $.notify(content, {
        clickToHide: true,
        position: 'bottom right',
        className: 'error'
    })
}

function warning(content) {
    $.notify(content, {
        clickToHide: true,
        position: 'bottom right',
        className: 'warning'
    })
}

// END NOTIFY JS

// START SWEET ALERT2
function confirmRemoveItem(productId, callbackSuccessFn) {
    Swal.fire({
        title: 'Xóa sản phẩm khỏi giỏ hàng',
        text: "Bạn có chắc chắn muốn xóa khỏi giỏ hàng không?",
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Không',
        confirmButtonText: 'Xóa sản phẩm!',
        customClass: {
            cancelButton: 'cancel-btn-remove-item',
            confirmButton: 'confirm-btn-remove-item'
        }
    }).then((result) => {
        if (result.isConfirmed) {
            removeItemCallback(productId, callbackSuccessFn);
        }
    })
}
// END SWEET ALERT 2