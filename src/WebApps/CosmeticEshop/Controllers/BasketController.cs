﻿using AutoMapper;

using CosmeticEshop.Extensions;
using CosmeticEshop.Models.Baskets;
using CosmeticEshop.Models.Order;
using CosmeticEshop.Services.Interfaces;
using CosmeticEshop.SignalR;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CosmeticEshop.Controllers
{
    public class BasketController : Controller
    {
        private readonly IBasketService _basketService;
        private readonly IProductService _productService;
        private readonly ISystemService _systemService;
        private readonly IMapper _mapper;
        private readonly IHubContext<BasketHub, IBasketHub> _hubContext;

        private readonly ILogger<BasketController> _logger;

        public BasketController(IBasketService basketService, IProductService productService, ISystemService systemService, ILogger<BasketController> logger, IMapper mapper, IHubContext<BasketHub, IBasketHub> hubContext)
        {
            _basketService = basketService;
            _productService = productService;
            _systemService = systemService;
            _logger = logger;
            _mapper = mapper;
            _hubContext = hubContext;
        }

        public async Task<IActionResult> Index()
        {
            var shopContact = await SessionExtensions.GetContactInformationAsync(HttpContext, _systemService);
            var menus = await SessionExtensions.GetMenusAsync(HttpContext, _systemService);
            var basket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);

            ViewBag.shopContact = shopContact;
            ViewBag.menus = menus;
            ViewBag.basket = basket;

            return View();
        }
        public async Task<IActionResult> SyncBasket()
        {
            var basket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);
            return PartialView("_MiniCartView", basket);
        }
        public async Task<IActionResult> GetBasket()
        {
            var basket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);
            return Json(basket);
        }

        public async Task<IActionResult> EmptyBasket()
        {
            var currentBasket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);

            var basket = new Basket
            {
                UserId = currentBasket.UserId
            };
            await _basketService.UpdateBasketAsync(basket);

            await _hubContext.Clients.Group(basket.UserId).SetBasket(basket.Items.Count);

            return NoContent();
        }
        
        public async Task<IActionResult> AddItemToBasket(BasketItemDto dto)
        {
            var basket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);

            if (basket.Items.Any(x => x.ProductId == dto.ProductId))
            {
                var currentItem = basket.Items.First(x => x.ProductId == dto.ProductId);
                currentItem.Quantity += dto.Quantity;
            } else
            {
                basket.Items.Add(_mapper.Map<BasketItem>(dto));
            }

            await _basketService.UpdateBasketAsync(basket);

            await _hubContext.Clients.Group(basket.UserId).SetBasket(basket.Items.Count);

            return NoContent();
        }
        public async Task<IActionResult> SetQuantityProductInBasket(BasketItemDto dto)
        {
            var basket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);

            if (basket.Items.Any(x => x.ProductId == dto.ProductId))
            {
                var currentItem = basket.Items.First(x => x.ProductId == dto.ProductId);
                currentItem.Quantity = dto.Quantity;
            } else
            {
                basket.Items.Add(_mapper.Map<BasketItem>(dto));
            }

            await _basketService.UpdateBasketAsync(basket);

            await _hubContext.Clients.Group(basket.UserId).SetBasket(basket.Items.Count);

            return NoContent();
        }
        
        public async Task<IActionResult> RemoveItemFromBasket(string productId)
        {
            var basket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);

            basket.Items.Remove(basket.Items.First(x => x.ProductId == productId));

            await _basketService.UpdateBasketAsync(basket);

            await _hubContext.Clients.Group(basket.UserId).SetBasket(basket.Items.Count);
            return NoContent();
        }

        public async Task<IActionResult> Checkout()
        {
            var shopContact = await SessionExtensions.GetContactInformationAsync(HttpContext, _systemService);
            var menus = await SessionExtensions.GetMenusAsync(HttpContext, _systemService);
            var basket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);

            ViewBag.shopContact = shopContact;
            ViewBag.menus = menus;
            ViewBag.basket = basket;

            var orderDetailModel = new OrderDetailModel
            {
                Items = _mapper.Map<IEnumerable<OrderDetail>>(basket.Items)
            };
            return View(orderDetailModel);
        }
        
        public async Task<IActionResult> OrderSuccess()
        {
            var shopContact = await SessionExtensions.GetContactInformationAsync(HttpContext, _systemService);
            var menus = await SessionExtensions.GetMenusAsync(HttpContext, _systemService);

            ViewBag.shopContact = shopContact;
            ViewBag.menus = menus;
            ViewBag.basket = new Basket();

            return View();
        }
        public async Task<IActionResult> Order(OrderDetailModel order)
        {
            if (!ModelState.IsValid)
            { // re-render the view when validation failed.
                var shopContact = await SessionExtensions.GetContactInformationAsync(HttpContext, _systemService);
                var menus = await SessionExtensions.GetMenusAsync(HttpContext, _systemService);
                var basket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);

                ViewBag.shopContact = shopContact;
                ViewBag.menus = menus;
                ViewBag.basket = basket;

                order.Items = _mapper.Map<IEnumerable<OrderDetail>>(basket.Items);

                return View(nameof(Checkout), order);
            } else
            {
                await _basketService.Checkout(order);

                return RedirectToAction(nameof(OrderSuccess));
            }
        }
    }
}
