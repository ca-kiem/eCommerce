﻿using CosmeticEshop.Extensions;
using CosmeticEshop.Models.Blogs;
using CosmeticEshop.Models.Product;
using CosmeticEshop.Services.Interfaces;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CosmeticEshop.Controllers
{
    public class ProductController : Controller
    {
        private readonly IBasketService _basketService;
        private readonly IProductService _productService;
        private readonly ISystemService _systemService;

        private readonly ILogger<ProductController> _logger;

        public ProductController(IBasketService basketService, IProductService productService, ISystemService systemService, ILogger<ProductController> logger)
        {
            _basketService = basketService;
            _productService = productService;
            _systemService = systemService;
            _logger = logger;
        }

        [Route("/product")]
        [Route("/product/haracell")]
        public async Task<IActionResult> Index(ProductSearchParams @params)
        {
            if (string.IsNullOrEmpty(@params.Category))
            {
                // category code not valid
                // redirect to homepage
                return Redirect("/");
            }
            var categories = await SessionExtensions.GetCategoryAsync(HttpContext, _systemService);
            var categoryId = categories.FirstOrDefault(x => x.Code == @params.Category)?.Id;

            if (string.IsNullOrEmpty(categoryId))
            {
                // category code not valid
                // redirect to homepage
                return Redirect("/");
            } else
            {
                // get all children category id
                @params.CategoryIds = FindAllChildren(current: categoryId, categories);
            }

            var shopContact = await SessionExtensions.GetContactInformationAsync(HttpContext, _systemService);
            var menus = await SessionExtensions.GetMenusAsync(HttpContext, _systemService);
            var basket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);

            ViewBag.shopContact = shopContact;
            ViewBag.menus = menus;
            ViewBag.basket = basket;

            var productsSearch = await _productService.SearchProductsAsync(@params);

            ViewBag.products = productsSearch;
            ViewBag.parentCategory = FindAllParent(categoryId, categories);
            return View(@params);
        }

        private static IEnumerable<string> FindAllChildren(string current, IEnumerable<CategoryModel> categories)
        {
            var dict = categories.Where(x => x.ParentId != null)
                .ToLookup(x => x.ParentId).ToDictionary(x => x.Key, x => x.Select(y => y));
            var queue = new Queue<string>();

            queue.Enqueue(current);

            while(queue.TryDequeue(out string categoryParentId))
            {
                yield return categoryParentId;

                if (dict.TryGetValue(categoryParentId, out IEnumerable<CategoryModel> children))
                {
                    foreach (var item in children)
                    {
                        queue.Enqueue(item.Id);
                    }
                }
            }
        }
        
        private static IEnumerable<CategoryModel> FindAllParent(string current, IEnumerable<CategoryModel> categories, bool includeCurrent = true)
        {
            var lstParent = new List<CategoryModel>();
            CategoryModel parent = null;
            var parentId = current;
            if (includeCurrent)
            {
                lstParent.Add(categories.FirstOrDefault(x => x.Id == current));
            }

            do
            {
                parent = categories.FirstOrDefault(x => x.Id == parentId);
                if (parent != null)
                {
                    parentId = parent.ParentId;
                    lstParent.Add(parent);
                }
            } while (parent != null);

            return lstParent;
        }

        public async Task<IActionResult> Detail(string id)
        {
            var shopContact = await SessionExtensions.GetContactInformationAsync(HttpContext, _systemService);
            var menus = await SessionExtensions.GetMenusAsync(HttpContext, _systemService);
            var basket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);
            var categories = await SessionExtensions.GetCategoryAsync(HttpContext, _systemService);

            ViewBag.shopContact = shopContact;
            ViewBag.menus = menus;
            ViewBag.basket = basket;

            var product = await _productService.GetDetailProductAsync(id);
            var relatedProducts = await _productService.GetRelatedProductsAsync(id);
            ViewBag.RelatedProducts = relatedProducts;
            ViewBag.parentCategory = FindAllParent(product.CategoryId, categories);

            return View(product);
        }

        public async Task<IActionResult> QuickView(string productId)
        {
            var product = await _productService.GetDetailProductAsync(productId);
            return PartialView(product);
        }
    }
}
