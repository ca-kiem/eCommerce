﻿using CosmeticEshop.Extensions;
using CosmeticEshop.Models.Blogs;
using CosmeticEshop.Services.Interfaces;
using CosmeticEshop.ViewModels;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System.Threading.Tasks;

namespace CosmeticEshop.Controllers
{
    public class BlogController : Controller
    {
        private readonly IBasketService _basketService;
        private readonly ISystemService _systemService;
        private readonly IBlogService _blogService;

        private readonly ILogger<BlogController> _logger;

        public BlogController(IBasketService basketService, ISystemService systemService, IBlogService blogService, ILogger<BlogController> logger)
        {
            _basketService = basketService;
            _systemService = systemService;
            _blogService = blogService;
            _logger = logger;
        }

        public async Task<IActionResult> Index(BlogSearchParams @params)
        {
            var shopContact = await SessionExtensions.GetContactInformationAsync(HttpContext, _systemService);
            var menus = await SessionExtensions.GetMenusAsync(HttpContext, _systemService);
            var basket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);

            ViewBag.shopContact = shopContact;
            ViewBag.menus = menus;
            ViewBag.basket = basket;

            var blogs = await _blogService.SearchBlogsAsync(@params: null);
            ViewBag.Blogs = blogs;
            return View(@params ?? new BlogSearchParams());
        }

        public async Task<IActionResult> Detail(int id)
        {
            var shopContact = await SessionExtensions.GetContactInformationAsync(HttpContext, _systemService);
            var menus = await SessionExtensions.GetMenusAsync(HttpContext, _systemService);
            var basket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);

            ViewBag.shopContact = shopContact;
            ViewBag.menus = menus;
            ViewBag.basket = basket;

            var blog = await _blogService.GetDetailAsync(id);
            var relatedBlogs = await _blogService.GetRelatedBlogsAsync(id);

            ViewBag.RelatedBlogs = relatedBlogs;
            return View(blog);
        }
    }
}
