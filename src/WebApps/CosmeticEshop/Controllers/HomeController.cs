﻿using CosmeticEshop.Extensions;
using CosmeticEshop.Models;
using CosmeticEshop.Models.Contact;
using CosmeticEshop.Services.Interfaces;
using CosmeticEshop.ViewModels;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System.Diagnostics;
using System.Threading.Tasks;

namespace CosmeticEshop.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBasketService _basketService;
        private readonly IProductService _productService;
        private readonly ISystemService _systemService;
        private readonly IBlogService _blogService;

        private readonly ILogger<HomeController> _logger;

        public HomeController(IBasketService basketService, IProductService productService, IBlogService blogService, ISystemService systemService, ILogger<HomeController> logger)
        {
            _basketService = basketService;
            _productService = productService;
            _systemService = systemService;
            _blogService = blogService;
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            var shopContact = await SessionExtensions.GetContactInformationAsync(HttpContext, _systemService);
            var menus = await SessionExtensions.GetMenusAsync(HttpContext, _systemService);
            var basket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);
            var testimonials = await SessionExtensions.GetTestimonialsAsync(HttpContext, _systemService);
            var blogs = await _blogService.GetLastestBlogsAsync();
            var products = await _productService.GetHomeProducts();

            var homePageModel = new HomePageViewModel
            {
                Menus = menus,
                Basket = basket,
                ShopContact = shopContact,
                Categories = products,
                Testimonials = testimonials,
                LastestBlogs = blogs
            };
            return View(homePageModel);
        }

        public async Task<IActionResult> Contact()
        {
            var shopContact = await _systemService.GetShopContactAsync();
            ViewBag.ShopContact = shopContact;
            return View(new MessageContactModel());
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
