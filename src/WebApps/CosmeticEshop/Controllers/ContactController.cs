﻿using CosmeticEshop.Extensions;
using CosmeticEshop.Models.Contact;
using CosmeticEshop.Services.Interfaces;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System.Text.Json;
using System.Threading.Tasks;

namespace CosmeticEshop.Controllers
{

    public class ContactController : Controller
    {
        private readonly IBasketService _basketService;
        private readonly ISystemService _systemService;
        private readonly IMessageContactService _contactService;

        private readonly ILogger<ContactController> _logger;

        public ContactController(IBasketService basketService, ISystemService systemService, IMessageContactService contactService, ILogger<ContactController> logger)
        {
            _basketService = basketService;
            _systemService = systemService;
            _contactService = contactService;
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            var shopContact = await SessionExtensions.GetContactInformationAsync(HttpContext, _systemService);
            var menus = await SessionExtensions.GetMenusAsync(HttpContext, _systemService);
            var basket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);

            ViewBag.shopContact = shopContact;
            ViewBag.menus = menus;
            ViewBag.basket = basket;

            return View(new MessageContactModel());
        }

        [HttpPost]
        public async Task<IActionResult> PostMessage(MessageContactModel contactModel)
        {
            var shopContact = await SessionExtensions.GetContactInformationAsync(HttpContext, _systemService);
            var menus = await SessionExtensions.GetMenusAsync(HttpContext, _systemService);
            var basket = await SessionExtensions.GetBasketAsync(HttpContext, _basketService);

            ViewBag.shopContact = shopContact;
            ViewBag.menus = menus;
            ViewBag.basket = basket;

            _logger.LogInformation($"[CosmeticEshop - {nameof(ContactController)}]: Save message with value: {JsonSerializer.Serialize(contactModel)}");
            await _contactService.SaveMessageContactAsync(contactModel);
            _logger.LogInformation($"[CosmeticEshop - {nameof(ContactController)}]: Save message success");

            return View("SendMessageSuccess");
        }
    }
}
