﻿using System;

namespace CosmeticEshop.Infrastructure
{
    public static class API
    {

        public static class Purchase
        {
            public static string AddItemToBasket(string baseUri) => $"{baseUri}/basket/items";
            public static string UpdateBasketItem(string baseUri) => $"{baseUri}/basket/items";

            public static string GetOrderDraft(string baseUri, string basketId) => $"{baseUri}/order/draft/{basketId}";
        }

        public static class Basket
        {
            public static string GetBasket(string baseUri, string userId) => $"{baseUri}/shopping/{userId}";
            public static string UpdateBasket(string baseUri) => $"{baseUri}/basket";
            public static string CheckoutBasket(string baseUri) => $"{baseUri}/basket/checkout";
            public static string CleanBasket(string baseUri, string basketId) => $"{baseUri}/basket/{basketId}";
        }
        public static class System
        {
            internal static string GetMenu(string baseUri) => $"{baseUri}/system/menu";

            internal static string GetShopInformation(string baseUri) => $"{baseUri}/system/ShopContact";

            internal static string GetCategories(string baseUri) => $"{baseUri}/system/Category";

            internal static string GetTestimonials(string baseUri) => $"{baseUri}/system/Testimonial";
        }

        public static class Product
        {
            internal static string GetHomeProducts(string baseUri) => $"{baseUri}/shopping/CategoryProducts";

            internal static string GetDetailProduct(string baseUri, string id) => $"{baseUri}/product/product/{id}";

            internal static string GetSearchProducts(string baseUri) => $"{baseUri}/product/product/Search";

            internal static string GetRelatedProducts(string baseUri, string id) => $"{baseUri}/product/product/RelatedProducts/{id}";
        }
        public static class Contact
        {
            internal static string SaveMessage(string baseUri) => $"{baseUri}/message/";

        }
        public static class Blog
        {
            internal static string GetBlogDetail(string baseUri, int id) => $"{baseUri}/blog/{id}";

            internal static string SearchBlogs(string baseUri) => $"{baseUri}/blog/search";

            internal static string GetRelatedBlogs(string baseUri, int id) => $"{baseUri}/blog/related/{id}";

            internal static string GetLastestBlogs(string baseUri) => $"{baseUri}/blog/getLastest";
        }

    }
}