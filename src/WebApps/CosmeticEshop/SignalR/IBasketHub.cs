﻿using CosmeticEshop.Models.Baskets;

using System.Threading.Tasks;

namespace CosmeticEshop.SignalR
{
    public interface IBasketHub
    {
        Task SetBasket(int totalItems);
    }
}