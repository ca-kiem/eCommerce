﻿using CosmeticEshop.Models.Baskets;

using Microsoft.AspNetCore.SignalR;

using System;
using System.Threading.Tasks;

namespace CosmeticEshop.SignalR
{
    public class BasketHub : Hub<IBasketHub>
    {
        public void Send(int totalItems)
        {
            Clients.All.SetBasket(totalItems);
        }

        public override async Task OnConnectedAsync()
        {
            var hasCurrentUserId = Context.GetHttpContext().Request.Cookies.TryGetValue("user_id", out string userId);
            if (!hasCurrentUserId)
            {
                userId = Guid.NewGuid().ToString();
                // set user id to cookies
                Context.GetHttpContext().Response.Cookies.Append("user_id", userId);
            }

            await Groups.AddToGroupAsync(Context.ConnectionId, userId);

            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var hasCurrentUserId = Context.GetHttpContext().Request.Cookies.TryGetValue("user_id", out string userId);
            if (!hasCurrentUserId)
            {
                userId = Guid.NewGuid().ToString();
                // set user id to cookies
                Context.GetHttpContext().Response.Cookies.Append("user_id", userId);
            }
            else
            {
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, userId);
            }

            await base.OnDisconnectedAsync(exception);
        }
    }
}
