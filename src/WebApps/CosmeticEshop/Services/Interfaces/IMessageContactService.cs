﻿using CosmeticEshop.Models.Contact;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CosmeticEshop.Services.Interfaces
{
    public interface IMessageContactService
    {
        Task SaveMessageContactAsync(MessageContactModel contactModel);
    }
}
