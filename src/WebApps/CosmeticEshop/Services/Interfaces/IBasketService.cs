﻿using CosmeticEshop.Models.Baskets;
using CosmeticEshop.Models.Order;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace CosmeticEshop.Services.Interfaces
{
    public interface IBasketService
    {
        Task<Basket> GetBasket(string userId);
        Task<Basket> UpdateBasketAsync(Basket basket);
        Task Checkout(OrderDetailModel checkoutOrder);
        Task<Basket> SetQuantities(string userId, Dictionary<string, int> quantities);
        Task<OrderDetailModel> GetOrderDraft(string basketId);
    }
}
