﻿using CosmeticEshop.Models.Contact;
using CosmeticEshop.Models.Menu;
using CosmeticEshop.Models.Product;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace CosmeticEshop.Services.Interfaces
{
    public interface ISystemService
    {
        Task<IEnumerable<MenuModel>> GetMenuAsync();
        Task<ContactInformationModel> GetShopContactAsync();
        Task<IEnumerable<CategoryModel>> GetCategoriesAsync();
        Task<IEnumerable<Testimonial>> GetTestimonialsAsync();
    }
}
