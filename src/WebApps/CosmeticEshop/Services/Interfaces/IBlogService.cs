﻿using CosmeticEshop.Models.Blogs;
using CosmeticEshop.Models.Helpers;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace CosmeticEshop.Services.Interfaces
{
    public interface IBlogService
    {
        Task<ReturnListDto<BlogCardModel>> SearchBlogsAsync(BlogSearchParams @params);
        Task<BlogDetailModel> GetDetailAsync(int id);
        Task<IEnumerable<BlogCardModel>> GetRelatedBlogsAsync(int id);
        Task<IEnumerable<BlogCardModel>> GetLastestBlogsAsync();
    }
}
