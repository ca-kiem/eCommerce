﻿using CosmeticEshop.Models.Blogs;
using CosmeticEshop.Models.Helpers;
using CosmeticEshop.Models.Product;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CosmeticEshop.Services.Interfaces
{
    public interface IProductService
    {
        Task<IEnumerable<CategoryProductsModel>> GetHomeProducts();
        Task<ReturnListDto<ProductCardModel>> SearchProductsAsync(ProductSearchParams @params);
        Task<ProductDetailModel> GetDetailProductAsync(string id);
        Task<IEnumerable<ProductCardModel>> GetRelatedProductsAsync(string id);
    }
}
