﻿using CosmeticEshop.Infrastructure;
using CosmeticEshop.Models.Contact;
using CosmeticEshop.Services.Interfaces;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CosmeticEshop.Services.Implements
{
    public class MessageContactService : IMessageContactService
    {
        private readonly HttpClient _apiClient;
        private readonly ILogger<MessageContactService> _logger;
        private readonly string _contactUrl;

        public MessageContactService(HttpClient apiClient, ILogger<MessageContactService> logger, IConfiguration configuration)
        {
            _apiClient = apiClient;
            _logger = logger;

            _contactUrl = $"{configuration.GetValue<string>("Apisettings:WebAppGw")}/api/v1/messagecontact";
        }

        public async Task SaveMessageContactAsync(MessageContactModel contactModel)
        {
            var uri = API.Contact.SaveMessage(_contactUrl);
            _logger.LogInformation("[SaveMessageContact] -> Calling {Uri} to save the message contact", uri);
            var content = new StringContent(JsonSerializer.Serialize(contactModel), Encoding.UTF8, "application/json");
            var response = await _apiClient.PostAsync(uri, content);
            response.EnsureSuccessStatusCode();

            _logger.LogInformation("[SaveMessageContact] -> response code {StatusCode}", response.StatusCode);

        }
    }
}
