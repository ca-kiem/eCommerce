﻿using CosmeticEshop.Extensions;
using CosmeticEshop.Infrastructure;
using CosmeticEshop.Models.Blogs;
using CosmeticEshop.Models.Helpers;
using CosmeticEshop.Models.Product;
using CosmeticEshop.Services.Interfaces;

using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CosmeticEshop.Services.Implements
{
    public class ProductService : IProductService
    {
        private readonly HttpClient _apiClient;
        private readonly ILogger<SystemService> _logger;
        private readonly string _productUrl;
        private readonly string _productAggregatorUrl;

        public ProductService(HttpClient apiClient, ILogger<SystemService> logger, IConfiguration configuration)
        {
            _apiClient = apiClient;
            _logger = logger;

            _productUrl = $"{configuration.GetValue<string>("Apisettings:WebAppGw")}/api/v1";
            _productAggregatorUrl = $"{configuration.GetValue<string>("Apisettings:ShoppingAggregator")}/api/v1";
        }

        public async Task<ProductDetailModel> GetDetailProductAsync(string id)
        {
            var uri = API.Product.GetDetailProduct(_productUrl, id);
            _logger.LogDebug("[GetDetailProduct] -> Calling {Uri} to get the detail product", uri);
            var response = await _apiClient.GetAsync(uri);
            _logger.LogDebug("[GetDetailProduct] -> response code {StatusCode}", response.StatusCode);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsModelAsync<ProductDetailModel>();
            }
            else
            {
                var responseString = await response.Content.ReadAsStringAsync();
                _logger.LogError($"[GetDetailProduct] -> response code {response.StatusCode} with message: {responseString}");

                throw new CosmeticEShopException("There was an error occured!");
            }
        }

        public async Task<IEnumerable<CategoryProductsModel>> GetHomeProducts()
        {
            var uri = API.Product.GetHomeProducts(_productAggregatorUrl);
            _logger.LogDebug("[GetHomeProducts] -> Calling {Uri} to get the category products in home page", uri);
            var response = await _apiClient.GetAsync(uri);
            _logger.LogDebug("[GetHomeProducts] -> response code {StatusCode}", response.StatusCode);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsListModelAsync<CategoryProductsModel>();
            }
            else
            {
                var responseString = await response.Content.ReadAsStringAsync();
                _logger.LogError($"[GetHomeProducts] -> response code {response.StatusCode} with message: {responseString}");

                throw new CosmeticEShopException("There was an error occured!");
            }
        }

        public async Task<IEnumerable<ProductCardModel>> GetRelatedProductsAsync(string id)
        {
            var uri = API.Product.GetRelatedProducts(_productUrl, id);
            _logger.LogInformation("[GetRelatedProducts] -> Calling {Uri} to get related products", uri);

            var response = await _apiClient.GetAsync(uri);

            _logger.LogInformation("[GetRelatedProducts] -> response code {StatusCode}", response.StatusCode);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsListModelAsync<ProductCardModel>();
            }
            else
            {
                var responseString = await response.Content.ReadAsStringAsync();
                _logger.LogError($"[GetRelatedProducts] -> response code {response.StatusCode} with message: {responseString}");

                throw new CosmeticEShopException("There was an error occured!");
            }
        }

        public async Task<ReturnListDto<ProductCardModel>> SearchProductsAsync(ProductSearchParams @params)
        {
            var uri = API.Product.GetSearchProducts(_productUrl);
            _logger.LogDebug("[SearchProducts] -> Calling {Uri} to get the category products in home page", uri);

            var routeParams = new List<KeyValuePair<string, string>>{
                new KeyValuePair<string, string>("fromPrice", @params.FromPrice?.ToString()),
                new KeyValuePair<string, string>("toPrice", @params.ToPrice?.ToString()),
                new KeyValuePair<string, string>("orderBy", @params.OrderBy),
                new KeyValuePair<string, string>("direction", @params.Direction),
                new KeyValuePair<string, string>("currentPage", @params.CurrentPage.ToString()),
                new KeyValuePair<string, string>("pageSize", @params.PageSize.ToString()),
            };

            routeParams.AddRange(from id in @params.CategoryIds
                                 select new KeyValuePair<string, string>("categoryIds", id)
                                 );

            var response = await _apiClient.GetAsync(QueryHelpers.AddQueryString(uri, routeParams));
            _logger.LogDebug("[SearchProducts] -> response code {StatusCode}", response.StatusCode);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsModelAsync<ReturnListDto<ProductCardModel>>();
            }
            else
            {
                var responseString = await response.Content.ReadAsStringAsync();
                _logger.LogError($"[SearchProducts] -> response code {response.StatusCode} with message: {responseString}");

                throw new CosmeticEShopException("There was an error occured!");
            }
        }
    }
}
