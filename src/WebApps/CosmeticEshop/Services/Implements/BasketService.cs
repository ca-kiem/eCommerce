﻿using CosmeticEshop.Extensions;
using CosmeticEshop.Infrastructure;
using CosmeticEshop.Models.Baskets;
using CosmeticEshop.Models.Order;
using CosmeticEshop.Services.Interfaces;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CosmeticEshop.Services.Implements
{
    public class BasketService : IBasketService
    {
        private readonly HttpClient _apiClient;
        private readonly ILogger<BasketService> _logger;
        private readonly string _basketByPassUrl;
        private readonly string _purchaseUrl;

        public BasketService(HttpClient httpClient, ILogger<BasketService> logger, IConfiguration configuration)
        {
            _apiClient = httpClient;
            _logger = logger;

            _basketByPassUrl = $"{configuration.GetValue<string>("Apisettings:WebAppGw")}/api/v1/basket";
            _purchaseUrl = $"{configuration.GetValue<string>("Apisettings:ShoppingAggregator")}/api/v1";
        }

        public async Task<Basket> GetBasket(string userId)
        {
            var uri = API.Basket.GetBasket(_purchaseUrl, userId);
            _logger.LogInformation("[GetBasket] -> Calling {Uri} to get the basket", uri);
            var response = await _apiClient.GetAsync(uri);
            _logger.LogInformation("[GetBasket] -> response code {StatusCode}", response.StatusCode);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsModelAsync<Basket>();
            }
            else
            {
                var responseString = await response.Content.ReadAsStringAsync();
                _logger.LogError($"[GetBasket] -> response code {response.StatusCode} with message: {responseString}");

                throw new CosmeticEShopException("There was an error occured!");
            }
        }

        public async Task<Basket> UpdateBasketAsync(Basket basket)
        {
            var uri = API.Basket.UpdateBasket(_basketByPassUrl);

            var basketContent = new StringContent(JsonSerializer.Serialize(basket), Encoding.UTF8, "application/json");

            var response = await _apiClient.PostAsync(uri, basketContent);

            response.EnsureSuccessStatusCode();

            return basket;
        }

        public async Task Checkout(OrderDetailModel checkoutOrder)
        {
            var uri = API.Basket.CheckoutBasket(_basketByPassUrl);
            var basketContent = new StringContent(JsonSerializer.Serialize(checkoutOrder), Encoding.UTF8, "application/json");

            _logger.LogInformation("Uri chechout {uri}", uri);

            var response = await _apiClient.PostAsync(uri, basketContent);

            response.EnsureSuccessStatusCode();
        }

        public async Task<Basket> SetQuantities(string userId, Dictionary<string, int> quantities)
        {
            var uri = API.Purchase.UpdateBasketItem(_purchaseUrl);

            var basketUpdate = new
            {
                BasketId = userId,
                Updates = quantities.Select(kvp => new
                {
                    BasketItemId = kvp.Key,
                    NewQty = kvp.Value
                }).ToArray()
            };

            var basketContent = new StringContent(JsonSerializer.Serialize(basketUpdate), Encoding.UTF8, "application/json");

            var response = await _apiClient.PutAsync(uri, basketContent);

            response.EnsureSuccessStatusCode();

            var jsonResponse = await response.Content.ReadAsStringAsync();

            return JsonSerializer.Deserialize<Basket>(jsonResponse);
        }

        public async Task<OrderDetailModel> GetOrderDraft(string basketId)
        {
            var uri = API.Purchase.GetOrderDraft(_purchaseUrl, basketId);

            var responseString = await _apiClient.GetStringAsync(uri);

            var response = JsonSerializer.Deserialize<OrderDetailModel>(responseString);

            return response;
        }

        public async Task AddItemToBasket(string userId, int productId)
        {
            var uri = API.Purchase.AddItemToBasket(_purchaseUrl);

            var newItem = new
            {
                CatalogItemId = productId,
                BasketId = userId,
                Quantity = 1
            };

            var basketContent = new StringContent(JsonSerializer.Serialize(newItem), Encoding.UTF8, "application/json");

            var response = await _apiClient.PostAsync(uri, basketContent);
        }
    }
}
