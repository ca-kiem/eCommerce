﻿using CosmeticEshop.Extensions;
using CosmeticEshop.Infrastructure;
using CosmeticEshop.Models.Blogs;
using CosmeticEshop.Models.Helpers;
using CosmeticEshop.Services.Interfaces;

using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace CosmeticEshop.Services.Implements
{
    public class BlogService : IBlogService
    {
        private readonly HttpClient _apiClient;
        private readonly ILogger<BlogService> _logger;
        private readonly string _blogUrl;

        public BlogService(HttpClient apiClient, ILogger<BlogService> logger, IConfiguration configuration)
        {
            _apiClient = apiClient;
            _logger = logger;
            _blogUrl = $"{configuration.GetValue<string>("Apisettings:WebAppGw")}/api/v1/blog";
        }

        public async Task<BlogDetailModel> GetDetailAsync(int id)
        {
            var uri = API.Blog.GetBlogDetail(_blogUrl, id);
            _logger.LogInformation("[GetDetailBlog] -> Calling {Uri} to search blog", uri);

            var response = await _apiClient.GetAsync(uri);

            _logger.LogInformation("[GetDetailBlog] -> response code {StatusCode}", response.StatusCode);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsModelAsync<BlogDetailModel>();
            }
            else
            {
                var responseString = await response.Content.ReadAsStringAsync();
                _logger.LogError($"[GetDetailBlog] -> response code {response.StatusCode} with message: {responseString}");

                throw new CosmeticEShopException("There was an error occured!");
            }
        }

        public async Task<IEnumerable<BlogCardModel>> GetLastestBlogsAsync()
        {
            var uri = API.Blog.GetLastestBlogs(_blogUrl);
            _logger.LogInformation("[GetLastestBlogs] -> Calling {Uri} to get lastest blogs", uri);

            var response = await _apiClient.GetAsync(uri);

            _logger.LogInformation("[GetLastestBlogs] -> response code {StatusCode}", response.StatusCode);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsListModelAsync<BlogCardModel>();
            }
            else
            {
                var responseString = await response.Content.ReadAsStringAsync();
                _logger.LogError($"[GetLastestBlogs] -> response code {response.StatusCode} with message: {responseString}");

                throw new CosmeticEShopException("There was an error occured!");
            }
        }

        public async Task<IEnumerable<BlogCardModel>> GetRelatedBlogsAsync(int id)
        {
            var uri = API.Blog.GetRelatedBlogs(_blogUrl, id);
            _logger.LogInformation("[GetRelatedBlogs] -> Calling {Uri} to get realted blogs", uri);

            var response = await _apiClient.GetAsync(uri);

            _logger.LogInformation("[GetRelatedBlogs] -> response code {StatusCode}", response.StatusCode);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsListModelAsync<BlogCardModel>();
            }
            else
            {
                var responseString = await response.Content.ReadAsStringAsync();
                _logger.LogError($"[GetRelatedBlogs] -> response code {response.StatusCode} with message: {responseString}");

                throw new CosmeticEShopException("There was an error occured!");
            }
        }

        public async Task<ReturnListDto<BlogCardModel>> SearchBlogsAsync(BlogSearchParams @params)
        {
            var uri = API.Blog.SearchBlogs(_blogUrl);
            _logger.LogInformation("[SearchBlogs] -> Calling {Uri} to search blog", uri);

            var query = new Dictionary<string, string>
            {
                ["content"] = @params?.Content,
                ["createdBy"] = @params?.CreatedBy,
                ["currentPage"] = @params == null ? "1" : @params.CurrentPage.ToString(),
                ["pageSize"] = @params == null ? "25" : @params.PageSize.ToString(),
            };


            var response = await _apiClient.GetAsync(QueryHelpers.AddQueryString(uri, query));

            _logger.LogInformation("[SearchBlogs] -> response code {StatusCode}", response.StatusCode);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsModelAsync<ReturnListDto<BlogCardModel>>();
            }
            else
            {
                var responseString = await response.Content.ReadAsStringAsync();
                _logger.LogError($"[SearchBlogs] -> response code {response.StatusCode} with message: {responseString}");

                throw new CosmeticEShopException("There was an error occured!");
            }
        }
    }
}
