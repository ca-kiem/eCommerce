﻿using CosmeticEshop.Extensions;
using CosmeticEshop.Infrastructure;
using CosmeticEshop.Models.Contact;
using CosmeticEshop.Models.Menu;
using CosmeticEshop.Models.Product;
using CosmeticEshop.Services.Interfaces;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace CosmeticEshop.Services.Implements
{
    public class SystemService : ISystemService
    {
        private readonly HttpClient _apiClient;
        private readonly ILogger<SystemService> _logger;
        private readonly string _systemUrl;

        public SystemService(HttpClient apiClient, ILogger<SystemService> logger, IConfiguration configuration)
        {
            _apiClient = apiClient;
            _logger = logger;

            _systemUrl = $"{configuration.GetValue<string>("Apisettings:WebAppGw")}/api/v1";
        }

        public async Task<IEnumerable<CategoryModel>> GetCategoriesAsync()
        {
            var uri = API.System.GetCategories(_systemUrl);
            _logger.LogInformation("[GetCategories] -> Calling {Uri} to get the categories", uri);
            var response = await _apiClient.GetAsync(uri);
            _logger.LogInformation("[GetCategories] -> response code {StatusCode}", response.StatusCode);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsListModelAsync<CategoryModel>();
            }
            else
            {
                var responseString = await response.Content.ReadAsStringAsync();
                _logger.LogError($"[GetCategories] -> response code {response.StatusCode} with message: {responseString}");

                throw new CosmeticEShopException("There was an error occured!");
            }
        }

        public async Task<IEnumerable<MenuModel>> GetMenuAsync()
        {
            var uri = API.System.GetMenu(_systemUrl);
            _logger.LogInformation("[GetMenu] -> Calling {Uri} to get the menus", uri);
            var response = await _apiClient.GetAsync(uri);
            _logger.LogInformation("[GetMenu] -> response code {StatusCode}", response.StatusCode);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsListModelAsync<MenuModel>();
            }
            else
            {
                var responseString = await response.Content.ReadAsStringAsync();
                _logger.LogError($"[GetMenu] -> response code {response.StatusCode} with message: {responseString}");

                throw new CosmeticEShopException("There was an error occured!");
            }
        }

        public async Task<ContactInformationModel> GetShopContactAsync()
        {
            var uri = API.System.GetShopInformation(_systemUrl);
            _logger.LogInformation("[GetShopInformation] -> Calling {Uri} to get the shop information", uri);
            var response = await _apiClient.GetAsync(uri);
            _logger.LogInformation("[GetShopInformation] -> response code {StatusCode}", response.StatusCode);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsModelAsync<ContactInformationModel>();
            }
            else
            {
                var responseString = await response.Content.ReadAsStringAsync();
                _logger.LogError($"[GetShopInformation] -> response code {response.StatusCode} with message: {responseString}");

                throw new CosmeticEShopException("There was an error occured!");
            }
        }

        public async Task<IEnumerable<Testimonial>> GetTestimonialsAsync()
        {
            var uri = API.System.GetTestimonials(_systemUrl);
            _logger.LogInformation("[GetTestimonials] -> Calling {Uri} to get the testimonials", uri);
            var response = await _apiClient.GetAsync(uri);
            _logger.LogInformation("[GetTestimonials] -> response code {StatusCode}", response.StatusCode);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsListModelAsync<Testimonial>();
            }
            else
            {
                var responseString = await response.Content.ReadAsStringAsync();
                _logger.LogError($"[GetTestimonials] -> response code {response.StatusCode} with message: {responseString}");

                throw new CosmeticEShopException("There was an error occured!");
            }
        }
    }
}
