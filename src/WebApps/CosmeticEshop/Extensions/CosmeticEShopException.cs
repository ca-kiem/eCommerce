﻿using System;
using System.Runtime.Serialization;

namespace CosmeticEshop.Extensions
{
    [Serializable]
    public class CosmeticEShopException : Exception
    {
        public CosmeticEShopException()
        {
        }

        public CosmeticEShopException(string message) : base(message)
        {
        }

        public CosmeticEShopException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CosmeticEShopException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
