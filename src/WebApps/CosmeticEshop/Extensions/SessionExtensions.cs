﻿using CosmeticEshop.Models.Baskets;
using CosmeticEshop.Models.Contact;
using CosmeticEshop.Models.Menu;
using CosmeticEshop.Models.Product;
using CosmeticEshop.Services.Interfaces;

using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace CosmeticEshop.Extensions
{
    public class SessionExtensions
    {
        protected SessionExtensions()
        {

        }

        /// <summary>
        /// Check shop contact existed in the session or not
        /// If not get from service and set to session
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="systemService"></param>
        /// <returns></returns>
        public static async Task<ContactInformationModel> GetContactInformationAsync(HttpContext httpContext, ISystemService systemService)
        {
            var hasShopContactValue = httpContext.Request.Cookies.TryGetValue("shop-contact", out string value);
            if (hasShopContactValue)
            {
                try
                {
                    return JsonSerializer.Deserialize<ContactInformationModel>(value);
                }
                catch (Exception)
                {
                    var shopContact = await systemService.GetShopContactAsync();
                    httpContext.Response.Cookies.Append("shop-contact", JsonSerializer.Serialize(shopContact));

                    return shopContact;
                }
            }
            else
            {
                var shopContact = await systemService.GetShopContactAsync();
                httpContext.Response.Cookies.Append("shop-contact", JsonSerializer.Serialize(shopContact));

                return shopContact;
            }
        }

        internal static async Task<IEnumerable<Testimonial>> GetTestimonialsAsync(HttpContext httpContext, ISystemService systemService)
        {
            var hasTestimonialsValue = httpContext.Request.Cookies.TryGetValue("testimonial", out string value);
            if (hasTestimonialsValue)
            {
                try
                {
                    return JsonSerializer.Deserialize<IEnumerable<Testimonial>>(value);
                }
                catch (Exception)
                {
                    var testimonials = await systemService.GetTestimonialsAsync();
                    httpContext.Response.Cookies.Append("testimonial", JsonSerializer.Serialize(testimonials));

                    return testimonials;
                }
            }
            else
            {
                var testimonials = await systemService.GetTestimonialsAsync();
                httpContext.Response.Cookies.Append("testimonial", JsonSerializer.Serialize(testimonials));

                return testimonials;
            }
        }

        /// <summary>
        /// Get shop phone
        /// If not get from service and set to session
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="systemService"></param>
        /// <returns></returns>
        public static string GetContactPhone(HttpContext httpContext)
        {
            var hasShopContactValue = httpContext.Request.Cookies.TryGetValue("shop-contact", out string value);
            if (hasShopContactValue)
            {
                try
                {
                    return JsonSerializer.Deserialize<ContactInformationModel>(value)?.PhoneNumber;
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        internal static async Task<IEnumerable<CategoryModel>> GetCategoryAsync(HttpContext httpContext, ISystemService systemService)
        {
            var hasShopContactValue = httpContext.Request.Cookies.TryGetValue("product-categories", out string value);
            if (hasShopContactValue)
            {
                try
                {
                    return JsonSerializer.Deserialize<IEnumerable<CategoryModel>>(value);
                }
                catch (Exception)
                {
                    var shopContact = await systemService.GetCategoriesAsync();
                    httpContext.Response.Cookies.Append("product-categories", JsonSerializer.Serialize(shopContact));

                    return shopContact;
                }
            }
            else
            {
                var shopContact = await systemService.GetCategoriesAsync();
                httpContext.Response.Cookies.Append("product-categories", JsonSerializer.Serialize(shopContact));

                return shopContact;
            }
        }

        /// <summary>
        /// Check menu existed in the session or not
        /// If not get from service and set to session
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="systemService"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<MenuModel>> GetMenusAsync(HttpContext httpContext, ISystemService systemService)
        {
            var hasShopContactValue = httpContext.Request.Cookies.TryGetValue("menus", out string value);
            if (hasShopContactValue)
            {
                try
                {
                    return JsonSerializer.Deserialize<IEnumerable<MenuModel>>(value);
                }
                catch (Exception)
                {
                    var menus = await systemService.GetMenuAsync();
                    httpContext.Response.Cookies.Append("menus", JsonSerializer.Serialize(menus));

                    return menus;
                }
            }
            else
            {
                var menus = await systemService.GetMenuAsync();
                httpContext.Response.Cookies.Append("menus", JsonSerializer.Serialize(menus));

                return menus;
            }
        }

        /// <summary>
        /// Check user id existed in the cookie or not
        /// If not create new user id and set in the cookie
        /// else Get basket from service
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="basketService"></param>
        /// <returns></returns>
        public static async Task<Basket> GetBasketAsync(HttpContext httpContext, IBasketService basketService)
        {
            var hasCurrentUserId = httpContext.Request.Cookies.TryGetValue("user_id", out string userId);
            Basket basket;
            if (!hasCurrentUserId)
            {
                userId = Guid.NewGuid().ToString();
                // set user id to cookies
                httpContext.Response.Cookies.Append("user_id", userId);

                basket = new Basket
                {
                    UserId = userId
                };
            }
            else
            {
                basket = await basketService.GetBasket(userId);
            }

            return basket;
        }
    }
}
