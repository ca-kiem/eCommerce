﻿namespace CosmeticEshop.Extensions
{
    public static class NumberFormat
    {
        public static string ToVNString(this decimal number)
        {
            return number.ToString("N0");
        }
    }
}
