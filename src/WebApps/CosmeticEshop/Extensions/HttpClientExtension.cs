﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace CosmeticEshop.Extensions
{
    public static class HttpClientExtension
    {
        public static async Task<IEnumerable<T>> ReadAsListModelAsync<T>(this HttpContent context) where T : class
        {
            var responseString = await context.ReadAsStringAsync();

            if (string.IsNullOrEmpty(responseString))
            {
                return new List<T>();
            } else
            {
                var serializeOptions = new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    //MaxDepth = 1
                };

                return JsonSerializer.Deserialize<IEnumerable<T>>(responseString, serializeOptions);
            }
        }
        
        public static async Task<T> ReadAsModelAsync<T>(this HttpContent context) where T : class
        {
            var responseString = await context.ReadAsStringAsync();

            if (string.IsNullOrEmpty(responseString))
            {
                return null;
            } else
            {
                var serializeOptions = new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                };

                return JsonSerializer.Deserialize<T>(responseString, serializeOptions);
            }
        }
    }
}
