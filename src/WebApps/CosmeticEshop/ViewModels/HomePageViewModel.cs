﻿using CosmeticEshop.Models.Baskets;
using CosmeticEshop.Models.Blogs;
using CosmeticEshop.Models.Contact;
using CosmeticEshop.Models.Menu;
using CosmeticEshop.Models.Product;

using System.Collections.Generic;

namespace CosmeticEshop.ViewModels
{
    public class HomePageViewModel
    {
        public HomePageViewModel()
        {
            Menus = new List<MenuModel>();
            Categories = new List<CategoryProductsModel>();
            LastestBlogs = new List<BlogCardModel>();
        }
        public IEnumerable<MenuModel> Menus { get; internal set; }
        public Basket Basket { get; internal set; }
        public ContactInformationModel ShopContact { get; internal set; }
        public IEnumerable<CategoryProductsModel> Categories { get; internal set; }
        public IEnumerable<BlogCardModel> LastestBlogs { get; internal set; }
        public IEnumerable<Testimonial> Testimonials { get; internal set; }
    }
}
