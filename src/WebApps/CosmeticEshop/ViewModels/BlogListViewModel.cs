﻿using CosmeticEshop.Models.Blogs;
using CosmeticEshop.Models.Helpers;

namespace CosmeticEshop.ViewModels
{
    public class BlogListViewModel
    {
        public ReturnListDto<BlogCardModel> Blogs { get; set; }
        public BlogSearchParams SearchParams { get; set; }
    }
}
