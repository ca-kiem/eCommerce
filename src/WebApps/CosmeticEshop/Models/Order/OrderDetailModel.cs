﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CosmeticEshop.Models.Order
{
    public class OrderDetailModel
    {
        public OrderDetailModel()
        {
            Items = new List<OrderDetail>();
        }
        // Ip address
        [MaxLength(50)]
        public string UserId { get; set; }
        public decimal TotalPrice { get; set; }

        // BillingAddress
        [MaxLength(50, ErrorMessage = "Tên không được vượt quá 50 ký tự!")]
        [Required(ErrorMessage = "Tên không được để trống!")]
        [Display(Name = "Tên")]
        public string BuyerFirstName { get; set; }
        [MaxLength(50, ErrorMessage = "Tên đệm không được vượt quá 50 ký tự!")]
        [Display(Name = "Họ và tên đệm")]
        public string BuyerLastName { get; set; }
        [MaxLength(15, ErrorMessage = "Số điện thoại không đúng!")]
        [Required(ErrorMessage = "Số điện thoại không được để trống!")]
        [Display(Name = "Số điện thoại")]
        [Phone(ErrorMessage = "Số điện thoại không đúng định dạng!")]
        public string BuyerPhoneNumber { get; set; }

        [Display(Name = "Địa chỉ Email")]
        [Required(ErrorMessage = "Email không được để trống!")]
        [EmailAddress(ErrorMessage = "Email không đúng định dạng!")]
        [MaxLength(255, ErrorMessage = "Email không được vượt quá 255 ký tự!")]
        public string BuyerEmailAddress { get; set; }
        [MaxLength(255, ErrorMessage = "Địa chỉ không được vượt quá 255 ký tự!")]
        [Required(ErrorMessage = "Địa chỉ không được để trống!")]
        [Display(Name = "Địa chỉ")]
        public string BuyerAddressLine { get; set; }
        [MaxLength(50, ErrorMessage = "Tên thành phố không được vượt quá 50 ký tự!")]
        [Display(Name = "Thành phố")]
        [Required(ErrorMessage = "Tỉnh/Thành phố không được để trống!")]
        public string BuyerState { get; set; }

        // shipping information
        [MaxLength(50, ErrorMessage = "Tên không được vượt quá 50 ký tự!")]
        [Required(ErrorMessage = "Tên không được để trống!")]
        [Display(Name = "Tên")]
        public string ReceiverFirstName { get; set; }
        [MaxLength(50, ErrorMessage = "Tên đệm không được vượt quá 50 ký tự!")]
        [Display(Name = "Họ và tên đệm")]
        public string ReceiverLastName { get; set; }
        [MaxLength(15, ErrorMessage = "Số điện thoại không đúng!")]
        [Required(ErrorMessage = "Số điện thoại không được để trống!")]
        [Display(Name = "Số điện thoại")]
        [Phone(ErrorMessage = "Số điện thoại không đúng định dạng!")]
        public string ReceiverPhoneNumber { get; set; }
        [Display(Name = "Địa chỉ Email")]
        [Required(ErrorMessage = "Email không được để trống!")]
        [EmailAddress(ErrorMessage = "Email không đúng định dạng!")]
        [MaxLength(255, ErrorMessage = "Email không được vượt quá 255 ký tự!")]
        public string ReceiverEmailAddress { get; set; }
        [MaxLength(255, ErrorMessage = "Địa chỉ không được vượt quá 255 ký tự!")]
        [Required(ErrorMessage = "Địa chỉ không được để trống!")]
        [Display(Name = "Địa chỉ")]
        public string ReceiverAddressLine { get; set; }
        [MaxLength(50, ErrorMessage = "Tên tỉnh/thành phố không được vượt quá 50 ký tự!")]
        [Display(Name = "Tỉnh/Thành phố")]
        [Required(ErrorMessage = "Tỉnh/Thành phố không được để trống!")]
        public string ReceiverState { get; set; }

        [MaxLength(255)]
        [Display(Name = "Ghi chú của đơn hàng")]
        public string Note { get; set; }
        // Payment
        public int PaymentMethod { get; set; }

        /// <summary>
        /// Is shipping address same as buyer address
        /// </summary>
        [Display(Name = "Giao hàng tới địa chỉ khác?")]
        public bool ShippingDifferentAddress { get; set; } = false;
        [Required(ErrorMessage = "Bạn cần đồng ý các điều khoản và thỏa thuận")]
        public bool AcceptPrivacy { get; set; } = false;
        /// <summary>
        /// Status of the order
        /// <para>0: Not process</para>
        /// <para>1: Processing</para>
        /// <para>2: Done</para>
        /// <para>3: Cancel</para>
        /// </summary>
        public int Status { get; set; } = 0;

        public virtual IEnumerable<OrderDetail> Items { get; set; }
    }
}
