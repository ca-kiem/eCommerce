﻿namespace CosmeticEshop.Models.Order
{
    public class OrderDetail
    {
        public string ProductId { get; init; }

        public string ProductName { get; init; }

        public decimal Price { get; init; }
        public decimal Sale { get; init; }

        public int Quantity { get; init; }

        public string PictureUrl { get; init; }
    }
}
