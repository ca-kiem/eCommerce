﻿using System.ComponentModel.DataAnnotations;

namespace CosmeticEshop.Models.Contact
{
    public class MessageContactModel
    {
        [Required(ErrorMessage = "Tên không được để trống!")]
        [MaxLength(50, ErrorMessage = "Tên không được vượt quá 50 ký tự!")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Email không được để trống!")]
        [EmailAddress(ErrorMessage = "Email không đúng định dạng!")]
        [MaxLength(255, ErrorMessage = "Email không được vượt quá 255 ký tự!")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Số điện thoại không được để trống!")]
        [Phone(ErrorMessage = "Số điện thoại không đúng định dạng!")]
        [MaxLength(15, ErrorMessage = "Số điện thoại không đúng định dạng!")]
        public string PhoneNumber { get; set; }
        [MaxLength(255, ErrorMessage = "Tin nhắn không được vượt quá 255 ký tự!")]
        [Required(ErrorMessage = "Hãy để lại thêm lời nhắn nhé!")]
        public string Message { get; set; }
    }
}
