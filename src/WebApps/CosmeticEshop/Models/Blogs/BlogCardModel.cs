﻿using System;

namespace CosmeticEshop.Models.Blogs
{
    public class BlogCardModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string ImagePath { get; set; }
        public string Author { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
