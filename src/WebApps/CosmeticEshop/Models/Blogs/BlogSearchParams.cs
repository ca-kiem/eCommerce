﻿namespace CosmeticEshop.Models.Blogs
{
    public class BlogSearchParams : BaseParams
    {
        public string Content { get; set; }
        public string CreatedBy { get; set; }
    }
}
