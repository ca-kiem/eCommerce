﻿using System.Collections.Generic;

namespace CosmeticEshop.Models.Helpers
{
    public class ReturnListDto<T>
    {
        public ReturnListDto()
        {
            Data = new List<T>();
        }
        public IEnumerable<T> Data { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages { get; set; }
    }
}
