﻿namespace CosmeticEshop.Models.Menu
{
    public class MenuModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string ParentId { get; set; }
        public int Order { get; set; }
    }
}
