﻿namespace CosmeticEshop.Models.Baskets
{
    public class BasketItemDto
    {
        public string ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
