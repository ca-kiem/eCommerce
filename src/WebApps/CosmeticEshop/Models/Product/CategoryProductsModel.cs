﻿using System.Collections.Generic;

namespace CosmeticEshop.Models.Product
{
    public class CategoryProductsModel
    {
        public CategoryProductsModel()
        {
            Products = new List<ProductCardModel>();
        }
        public string CatagoryName { get; set; }
        public IEnumerable<ProductCardModel> Products { get; set; }
    }
}
