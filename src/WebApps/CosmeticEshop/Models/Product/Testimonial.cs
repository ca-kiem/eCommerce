﻿namespace CosmeticEshop.Models.Product
{
    public class Testimonial
    {
        public string Id { get; set; }
        public string Quote { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
