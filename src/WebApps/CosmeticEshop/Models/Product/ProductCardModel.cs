﻿namespace CosmeticEshop.Models.Product
{
    public class ProductCardModel
    {
        public string Id { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public string Summary { get; set; }
        public decimal Price { get; set; }
        public bool Status { get; set; }
        public decimal Sale { get; set; }
        public string ImageCardUrl { get; set; }
    }
}
