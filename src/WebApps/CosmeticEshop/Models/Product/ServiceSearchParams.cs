﻿using System.Collections.Generic;

namespace CosmeticEshop.Models.Product
{
    public class ServiceSearchParams : BaseParams
    {
        public ServiceSearchParams()
        {
            CategoryIds = new List<string>();
        }
        /// <summary>
        /// Search category code
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// all categories include current and its children
        /// </summary>
        public IEnumerable<string> CategoryIds { get; set; }
        public string Name { get; set; }
    }
}
