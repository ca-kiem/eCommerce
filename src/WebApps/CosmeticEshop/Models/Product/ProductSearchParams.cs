﻿using System.Collections.Generic;

namespace CosmeticEshop.Models.Product
{
    public class ProductSearchParams : BaseParams
    {
        public ProductSearchParams()
        {
            CategoryIds = new List<string>();
        }
        /// <summary>
        /// Search category code
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// all categories include current and its children
        /// </summary>
        public IEnumerable<string> CategoryIds { get; set; }
        public string Name { get; set; }
        public decimal? FromPrice { get; set; }
        public decimal? ToPrice { get; set; }
        public string OrderBy { get; set; }
        public string Direction { get; set; }
    }
}
