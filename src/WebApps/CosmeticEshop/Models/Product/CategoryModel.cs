﻿namespace CosmeticEshop.Models.Product
{
    public class CategoryModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
        public string Code { get; set; }

        public string ParentId { get; set; }
    }
}
