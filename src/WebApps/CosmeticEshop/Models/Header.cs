﻿using System.Collections.Generic;

namespace CosmeticEshop.Models
{
    public record Header
    {
        public Header()
        {
            RouteParams = new Dictionary<string, string>();
        }
        public string Controller { get; set; }
        public string Text { get; set; }
        public string Action { get; set; } = "Index";
        public string Area { get; set; }
        public bool Current { get; set; }
        public IDictionary<string, string> RouteParams { get; set; }
    }
}
