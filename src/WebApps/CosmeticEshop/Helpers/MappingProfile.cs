﻿using AutoMapper;

using CosmeticEshop.Models.Baskets;
using CosmeticEshop.Models.Order;
using CosmeticEshop.Models.Product;

namespace CosmeticEshop.Helpers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ServiceSearchParams, ProductSearchParams>();

            CreateMap<BasketItemDto, BasketItem>();
            CreateMap<BasketItem, OrderDetail>();
        }
    }
}
