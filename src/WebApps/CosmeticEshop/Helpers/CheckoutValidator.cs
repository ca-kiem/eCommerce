﻿using CosmeticEshop.Models.Order;

using FluentValidation;

namespace CosmeticEshop.Helpers
{
    public class CheckoutValidator : AbstractValidator<OrderDetailModel>
    {
        public CheckoutValidator()
        {
            RuleFor(order => order.ReceiverAddressLine).NotNull().When(x => x.ShippingDifferentAddress)
                .WithMessage("Địa chỉ không được để trống");

            RuleFor(order => order.ReceiverEmailAddress).NotNull().When(x => x.ShippingDifferentAddress)
                .WithMessage("Email không được để trống");

            RuleFor(order => order.ReceiverFirstName).NotNull().When(x => x.ShippingDifferentAddress)
                .WithMessage("Tên không được để trống");

            RuleFor(order => order.ReceiverPhoneNumber).NotNull().When(x => x.ShippingDifferentAddress)
                .WithMessage("Số điện thoại không được để trống");

            RuleFor(order => order.ReceiverState).NotNull().When(x => x.ShippingDifferentAddress)
                .WithMessage("Tỉnh/Thành phố không được để trống");

        }
    }
}
