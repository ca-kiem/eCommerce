﻿using CosmeticEshop.Services.Implements;
using CosmeticEshop.Services.Interfaces;

using Microsoft.Extensions.DependencyInjection;

namespace CosmeticEshop.Helpers
{
    public static class DIHelper
    {
        internal static IServiceCollection InitDI(this IServiceCollection services)
        {
            services.AddScoped<IBlogService, BlogService>();
            services.AddScoped<IBasketService, BasketService>();
            services.AddScoped<IMessageContactService, MessageContactService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<ISystemService, SystemService>();

            return services;
        }
    }
}
